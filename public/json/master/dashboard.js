$(document).ready(function() {
    $('#dataTableBuilder').DataTable({
        dom: 'lBfrtip',
        buttons: [
            { extend: 'copy', className: 'btn-sm' },
            { extend: 'csv', className: 'btn-sm' },
            { extend: 'excel', className: 'btn-sm' },
            { extend: 'pdf', className: 'btn-sm' },
            { extend: 'print', className: 'btn-sm' }
        ],
        responsive: true,
        'ajax': {
            'url': '/admin/getdatapopuler',
        },
        'columnDefs': [
            {
                'targets':0,
                'sClass': "text-center col-md-1"
            },
            {
                'targets':1,
                'sClass': "text-center col-md-2",
                'render': function (data, type, full, meta) {
                    var kembali = '';
                           kembali += '<img src="../images/product/'+data+'" style="width:40%;">';
                                return kembali;
                }
            },
            {
                'targets':2,
                'sClass': "text-center col-md-3"
            },{
                'targets':3,
                'sClass': "text-right col-md-2"
            },
            {
                'targets':4,
                'sClass': "text-right col-md-2"
            },
            {
              'targets': 5,
	            'searchable': false,
	            "orderable": false,
	            "orderData": false,
	            "orderDataType": false,
	            "orderSequence": false,
	            "sClass": "text-center col-md-2 td-aksi",
              'render': function (data, type, full, meta) {
	                var kembali = '';
 		                   kembali += '<button title="Hapus Data" class="btn btn-danger" data-toggle="modal" data-target="#modalHapus" onclick="HapusClick(this);"><li class="fa fa-trash-o"></li></button>';
	  			             kembali += '<button title="Ubah Data" class="btn btn-primary" onclick="UbahClick(this);"><li class="fa fa-pencil"></li></button>';  
								return kembali;
	            }
	           
	        }
	    ],
        'rowCallback': function (row, data, dataIndex) {

            $(row).find('button[class="btn btn-danger"]').prop('value', data[5]);
					  $(row).find('button[class="btn btn-primary"]').prop('value', data[5]);
        }
    });
});

function reloadTable() {
    var table = $('#dataTableBuilder').dataTable();
    table.cleanData;
    table.api().ajax.reload();
}

function UbahClick(btn) {
    var route = "/admin/product/edit/" + btn.value;
    window.location = route;
}

function HapusClick(btn) {
    $('#idHapus').val(btn.value);
}

$('#yakinhapus').click(function () {
    var token = $('#token').val();
    var id = $('#idHapus').val();
    var route = "/admin/product/delete/" + id;
		reloadTable();
    window.location = route;
});


var route3 = "/admin/getdatacategory";
var inputTipe = $('#category');

var list = document.getElementById("category");
while (list.hasChildNodes()) {
        list.removeChild(list.firstChild);
}
inputTipe.append('<option value=" ">Pilih Category</option>');

$.get(route3, function (res) {
        // console.log(res);
        $.each(res.data, function (index, value) {
                inputTipe.append('<option value="' + value[2] + '">' + value[0] + '</option>');
        });
});

$("#category").select();
