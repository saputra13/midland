$(document).ready(function() {
    $('#dataTableBuilder').DataTable({
        dom: 'lBfrtip',
        buttons: [
            { extend: 'copy', className: 'btn-sm' },
            { extend: 'csv', className: 'btn-sm' },
            { extend: 'excel', className: 'btn-sm' },
            { extend: 'pdf', className: 'btn-sm' },
            { extend: 'print', className: 'btn-sm' }
        ],
        responsive: true,
        'ajax': {
            'url': '/admin/getdatacluster',
        },
        'columnDefs': [
            {
                'targets':0,
                'sClass': "text-center col-md-3"
            },{
              'targets': 1,
	            'searchable': false,
	            "orderable": false,
	            "orderData": false,
	            "orderDataType": false,
	            "orderSequence": false,
	            "sClass": "text-center col-md-2 td-aksi",
              'render': function (data, type, full, meta) {
	                var kembali = '';
 		                   kembali += '<button title="Hapus Data" class="btn btn-danger" data-toggle="modal" data-target="#modalHapus" onclick="HapusClick(this);">HAPUS</button>';
	  			             kembali += '<button title="Ubah Data" class="btn btn-primary" onclick="UbahClick(this);">EDIT</button>';  
								return kembali;
	            }
	           
	        }
	    ],
        'rowCallback': function (row, data, dataIndex) {

            $(row).find('button[class="btn btn-danger"]').prop('value', data[1]);
					  $(row).find('button[class="btn btn-primary"]').prop('value', data[1]);
        }
    });
});

function reloadTable() {
    var table = $('#dataTableBuilder').dataTable();
    table.cleanData;
    table.api().ajax.reload();
}

function UbahClick(btn) {
    var route = "/admin/cluster/edit/" + btn.value;
    window.location = route;
}

function HapusClick(btn) {
    $('#idHapus').val(btn.value);
}

$('#yakinhapus').click(function () {
    var token = $('#token').val();
    var id = $('#idHapus').val();
    var route = "/admin/cluster/delete/" + id;
		reloadTable();
    window.location = route;
});

