$(document).ready(function() {
    $('#dataTableBuilder').DataTable({
        dom: 'lBfrtip',
        buttons: [
            { extend: 'copy', className: 'btn-sm' },
            { extend: 'csv', className: 'btn-sm' },
            { extend: 'excel', className: 'btn-sm' },
            { extend: 'pdf', className: 'btn-sm' },
            { extend: 'print', className: 'btn-sm' }
        ],
        responsive: true,
        'ajax': {
            'url': '/admin/getdatamember',
        },
        'columnDefs': [
            {
                'targets':0,
                'sClass': "col-md-3"
            },{
                'targets':1,
                'sClass': "col-md-2"
            },{
                'targets':2,
                'sClass': "col-md-2"
            },{
                'targets':3,
                'sClass': "col-md-2"
            },{
                'targets':4,
                'sClass': "text-center col-md-1",
								'render': function (data, type, row, meta) {
													 if(type === 'display'){
																if(data == "Aktif"){
																	 data = '<span class="label label-primary"> Aktif </span>';
																} else if(data == "Tidak Aktif"){
																	 data = '<span class="label label-warning"> Tidak Aktif </span>';
																}
														 }
														 return data;
										}
            },{
              'targets': 5,
	            'searchable': false,
	            "orderable": false,
	            "orderData": false,
	            "orderDataType": false,
	            "orderSequence": false,
	            "sClass": "text-center col-md-2 td-aksi",
              'render': function (data, type, full, meta) {
	                var kembali = '';
 		                    kembali += '<a title="Lihat Member" class="btn btn-info btn-flat" href="/admin/member/'+data+'">Detail</a>';
								return kembali;
	            }
	           
	        }
	    ],
        'rowCallback': function (row, data, dataIndex) {
					  $(row).find('button[class="btn btn-primary"]').prop('value', data[5]);
        }
    });
});

