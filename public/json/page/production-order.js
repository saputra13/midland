$(document).ready(function() {
    $('#dataTableBuilder').DataTable({
        dom: 'lBfrtip',
        buttons: [
            { extend: 'copy', className: 'btn-sm' },
            { extend: 'csv', className: 'btn-sm' },
            { extend: 'excel', className: 'btn-sm' },
            { extend: 'pdf', className: 'btn-sm' },
            { extend: 'print', className: 'btn-sm' }
        ],
        responsive: true,
        'ajax': {
            'url': '/admin/getdataproductionorder',
        },
        'columnDefs': [
            {
                'targets':0,
                'sClass': "text-center col-md-2"
            },{
                'targets':1,
                'sClass': "text-center col-md-2"
            },{
                'targets':2,
                'sClass': "col-md-2"
            },{
                'targets':3,
                'sClass': "col-md-2"
            },{
                'targets':4,
                'sClass': "text-center col-md-1",
								'render': function (data, type, row, meta) {
													 if(type === 'display'){
																if(data == "ORDER"){
																	 data = '<span class="label label-primary"> ORDER </span>';
																} else if(data == "TERBAYAR"){
                                                                     data = '<span class="label label-success"> TERBAYAR </span>';
                                                                }else if(data == "PRODUCTION"){
																	 data = '<span class="label label-warning"> PRODUKSI </span>';
																}else if(data == "KIRIM"){
                                                                     data = '<span class="label label-success"> KIRIM </span>';
                                                                }else if(data == "SAMPAI"){
                                                                     data = '<span class="label label-success"> SAMPAI </span>';
                                                                }
														 }
														 return data;
										}
            },{
              'targets': 5,
	            'searchable': false,
	            "orderable": false,
	            "orderData": false,
	            "orderDataType": false,
	            "orderSequence": false,
	            "sClass": "text-center col-md-2 td-aksi",
              'render': function (data, type, full, meta) {
	                var kembali = '';
 		                   kembali += '<a href="/admin/production-detail-product/spk/'+data+'" class="btn btn-warning" ><li class="fa fa-print"></li></button>';
						return kembali;
	            }
	        }
					,{
              'targets': 6,
	            'searchable': false,
	            "orderable": false,
	            "orderData": false,
	            "orderDataType": false,
	            "orderSequence": false,
	            "sClass": "text-center col-md-2 td-aksi",
              'render': function (data, type, full, meta) {
	                var kembali = '';
 		                   kembali += '<button title="Hapus Data" class="btn btn-success" onclick="detail(this);"><li class="fa fa-eye"></li></button>';
						return kembali;
	            }
	        }
	    ],
        'rowCallback': function (row, data, dataIndex) {
            $(row).find('button[class="btn btn-success"]').prop('value', data[5]);
        }
    });
});

function reloadTable() {
    var table = $('#dataTableBuilder').dataTable();
    table.cleanData;
    table.api().ajax.reload();
}

function detail(btn) {
    var route = "/admin/production-detail/" + btn.value;
    window.location = route;
}

