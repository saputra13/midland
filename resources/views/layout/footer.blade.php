        <div class="section-no-margin">
          <footer class="footer footer-style-1">
            <div class="footer-top">
              <div class="container"><a href="index.html" title="YOLO"><img src="/images/logo.png" alt="logo" class="logo-img" style="width: 20%;"/></a>
                <ul class="social-footer">
                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                  <li><a href="#"><i class="fa fa-tumblr"></i></a></li>
                  <li><a href="#"><i class="fa fa-rss"></i></a></li>
                </ul>
                <p class="desc">Be able to touch a classic furniture luxury is a little 'how to enter body and soul in a dream: for this reason Vimercati has set up in Italy a large exhibition area available to all customers who wish to admire these creations for them self and evaluate directly, the result of production and the visual impact.</p>
                <div class="div-icon-box">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="icon-box icon-box-style2">
                        <div class="icon-box-left"><i class="fa fa-map-marker"></i></div>
                        <div class="icon-box-right"><span>Odessa, Texas</span></div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="icon-box icon-box-style2">
                        <div class="icon-box-left"><i class="fa fa-phone"></i></div>
                        <div class="icon-box-right"><span>Phone : +1 (818) 357-7631</span></div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="icon-box icon-box-style2">
                        <div class="icon-box-left"><i class="fa fa-envelope-o"></i></div>
                        <div class="icon-box-right"><span>Email : sales@midlandhomefurniture.com</span></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="footer-bottom">
              <div class="container">
                <div class="copyright">©  2018  Midland Home Furniture  Designed  with by  <a href="http://theprime.id">theprime.id</a></div>
                <ul class="pay-footer">
                  <li><a href="#"><i class="fa fa-credit-card"></i></a></li>
                  <li><a href="#"><i class="fa fa-cc-visa"></i></a></li>
                  <li><a href="#"><i class="fa fa-cc-mastercard"></i></a></li>
                  <li><a href="#"><i class="fa fa-cc-amex"></i></a></li>
                  <li><a href="#"><i class="fa fa-cc-paypal"></i></a></li>
                </ul>
              </div>
            </div>
          </footer>
          <!-- .footer-->
        </div>