<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>MIDLAND HOME FURNITURE</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon-->
    <link rel="shortcut icon" href="images/icon/favicon.png" type="image/x-icon">

    <!-- Web Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i|Montserrat:400,700|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Ubuntu:300,300i,400,400i,500,500i,700,700i&amp;amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">

    <!-- Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/front_new/libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/front_new/libs/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/front_new/libs/animate/animated.css">
    <link rel="stylesheet" type="text/css" href="/front_new/libs/owl.carousel.min/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="/front_new/libs/jquery.mmenu.all/jquery.mmenu.all.css">
    <link rel="stylesheet" type="text/css" href="/front_new/libs/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" type="text/css" href="/front_new/libs/direction/css/noJS.css">
    <link rel="stylesheet" type="text/css" href="/front_new/libs/prettyphoto-master/css/prettyPhoto.css">
    <link rel="stylesheet" type="text/css" href="/front_new/libs/slick-sider/slick.min.css">

    <!-- Template CSS-->
    <link rel="stylesheet" type="text/css" href="/front_new/css/main.css">
    <link rel="stylesheet" type="text/css" href="/front_new/css/home.css">
    @yield('custom_style')
    @yield('custom_script')
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries-->
    <!-- WARNING: Respond.js doesn't work if you view the page via file://-->
    <!--if lt IE 9
    script(src='https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js')
    script(src='https://oss.maxcdn.com/respond/1.4.2/respond.min.js')
    
    -->
  </head>
  @yield('body')
    <div id="preloaderKDZ"></div>
    <div class="sn-site">
        <!-- begin Header -->
         @include('layout.header')    
         <!-- End Header -->
         <!-- begin content -->
         @yield('content')    
         <!-- End CONTENT -->
         <!-- End Content -->
         @include('layout.footer')    
         <!-- End Footer -->
         <!-- End Newsletter Popup -->
      
    </div>
    <!-- .mv-site-->
    
    

    <div class="popup-wrapper">
    </div>
    <!-- .popup-wrapper-->
    <div class="click-back-top-body">
      <button type="button" class="sn-btn sn-btn-style-17 sn-back-to-top fixed-right-bottom"><i class="btn-icon fa fa-angle-up"></i></button>
    </div>

    <!-- Vendor jQuery-->
    



    <script type="text/javascript" src="/front_new/libs/jquery/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="/front_new/libs/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/front_new/libs/animate/wow.min.js"></script>
    <script type="text/javascript" src="/front_new/libs/owl.carousel.min/owl.carousel.min.js"></script>
    <script type="text/javascript" src="/front_new/libs/jquery.mmenu.all/jquery.mmenu.all.min.js"></script>
    <script type="text/javascript" src="/front_new/libs/countdown/jquery.countdown.min.js"></script>
    <script type="text/javascript" src="/front_new/libs/jquery-appear/jquery.appear.min.js"></script>
    <script type="text/javascript" src="/front_new/libs/jquery-countto/jquery.countTo.min.js"></script>
    <script type="text/javascript" src="/front_new/libs/direction/js/jquery.hoverdir.js"></script>
    <script type="text/javascript" src="/front_new/libs/direction/js/modernizr.custom.97074.js"></script>
    <script type="text/javascript" src="/front_new/libs/isotope/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="/front_new/libs/isotope/fit-columns.js"></script>
    <script type="text/javascript" src="/front_new/libs/isotope/isotope-docs.min.js"></script>
    <script type="text/javascript" src="/front_new/libs/mansory/mansory.js"></script>
    <script type="text/javascript" src="/front_new/libs/prettyphoto-master/js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="/front_new/libs/slick-sider/slick.min.js"></script>
    <script type="text/javascript" src="/front_new/js/min/main.min.js"></script>
  </body>
</html>