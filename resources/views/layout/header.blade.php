<header class="header sn-header-style-1">
   <div class="header-top">
      <div class="container">
         <div class="header-top-left">
            <aside id="text-2" class="widget widget_text">
               <div class="textwidget"><i class="fa fa-envelope-o"></i>sales@midlandhomefurniture.com</div>
            </aside>
            <aside id="text-3" class="widget widget_text">
               <div class="textwidget"><i class="fa fa-mobile"></i>+1 (818) 357-7631</div>
            </aside>
            <aside id="text-4" class="widget widget_text">
               <div class="textwidget"><i class="fa fa-clock-o"></i> Mon - Fri: 9.00 - 17.00 </div>
            </aside>
            <aside id="text-4" class="widget widget_text">
               <div class="textwidget">
                  <a href="{{url('/profile')}}"> Our Profile</a>
               </div>
            </aside>
         </div>
      </div>
   </div><a href="#primary-menu"><i class="fa fa-bars"></i></a>
   <div class="container">
      <div class="header-bottom">

         <div class="main-nav-wrapper header-left">
            <div class="header-logo pull-left"><a href="/" title="MIDLAND"><img src="/images/logo.png" alt="logo" class="logo-img"
                     style="max-width: 10%;" /></a></div>
            <!-- .header-logo-->

            <nav id="primary-menu" class="main-nav">
               <ul class="nav">
                  <li class="active"><a href="{{url('/')}}">Home</a>
                     @foreach($menu as $x)
                  <li class="mega-menu menu-item">
                     <a href="/catalog/category/{{$x['name']}}">{{$x['name']}}</a>
                  </li>
                  @endforeach
               </ul>
            </nav>
            <!-- .header-main-nav-->
         </div>

         <div class="main-nav-wrapper header-right">
            <div class="header-right-box">
               <div class="header-customize header-customize-right">
                  <div class="shopping-cart-wrapper header-customize-item no-price style-default">
                     <a href="/cart" class="widget_shopping_cart_content">
                        <div class="widget_shopping_cart_icon"><i class="wicon fa fa-cart-plus"></i><span class="total">{{Cart::Content()->count()}}</span></div>
                     </a>
                  </div>
               </div>
               <div class="header-customize header-customize-right" style="margin-left: 50px;">
                  <div class="shopping-cart-wrapper header-customize-item no-price style-default">
                     <a href="/cart" class="widget_shopping_cart_content">
                        <div class="widget_shopping_cart_icon"><i class="wicon fa fa-user"></i></div>
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</header>

<div id="modal-success" class="modal modal-message modal-success fade" style="display: none;" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <center><i class="fa fa-key"></i></center>
         </div>
         <div class="modal-title">
            <center>
               <h2>REGISTER</h2>
            </center>
         </div>
         <form class="form-horizontal" role="form" method="POST" action="{{ url('/member/login') }}">
            <div class="modal-body">
               {{ csrf_field() }}
               <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label for="email" class="col-md-4 control-label">E-Mail Address</label>
                  <div class="col-md-6">
                     <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                        autofocus>
                     @if ($errors->has('email'))
                     <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                     </span>
                     @endif
                  </div>
               </div>
               <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <label for="password" class="col-md-4 control-label">Password</label>
                  <div class="col-md-6">
                     <input id="password" type="password" class="form-control" name="password">
                     @if ($errors->has('password'))
                     <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                     </span>
                     @endif
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <div class="col-md-6 col-md-offset-4">
                  <div class="checkbox">
                     <label>
                        <input type="checkbox" name="remember"> Remember Me
                     </label>
                  </div>
               </div>
               <button type="submit" class="btn btn-primary">
                  Login
               </button>
               <hr />
               <div class="col-md-12">
                  <h3> <a href="#" data-toggle="modal" data-target="#modal-register">NEED ACCOUNT ? REGISTER HERE</a></h3>
               </div>
            </div>
         </form>
      </div>
      <!-- / .modal-content -->
   </div>
   <!-- / .modal-dialog -->
</div>
<div id="modal-register" class="modal modal-message modal-success fade" style="display: none;" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <center><i class="fa fa-key"></i></center>
         </div>
         <div class="modal-title">
            <center>
               <h2>REGISTER</h2>
            </center>
         </div>
         <form class="form-horizontal" role="form" method="POST" action="{{ url('/member/register') }}">
            <div class="modal-body">
               {{ csrf_field() }}
               <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                  <label for="name" class="col-md-4 control-label">Name</label>
                  <div class="col-md-6">
                     <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" autofocus>
                     @if ($errors->has('name'))
                     <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                     </span>
                     @endif
                  </div>
               </div>
               <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label for="email" class="col-md-4 control-label">E-Mail Address</label>
                  <div class="col-md-6">
                     <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                     @if ($errors->has('email'))
                     <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                     </span>
                     @endif
                  </div>
               </div>
               <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <label for="password" class="col-md-4 control-label">Password</label>
                  <div class="col-md-6">
                     <input id="password" type="password" class="form-control" name="password">
                     @if ($errors->has('password'))
                     <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                     </span>
                     @endif
                  </div>
               </div>
               <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                  <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                  <div class="col-md-6">
                     <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                     @if ($errors->has('password_confirmation'))
                     <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                     </span>
                     @endif
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <div class="col-md-12 col-md-offset-4">
                  <center>
                     <button type="submit" class="btn btn-primary">
                        REGISTER
                     </button>
                  </center>
               </div>
            </div>
         </form>
      </div>
      <!-- / .modal-content -->
   </div>
   <!-- / .modal-dialog -->
</div>