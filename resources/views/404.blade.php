@extends('layout.master')

@section('title_name')
404
@endsection

@section('body')

<body class="page-404">
  @endsection

  @section('content')
  <div class="popup-wrapper">
    <div id="example-wrapper">
      <div class="section">
        <figure class="img-404"><img src="images/demo/page404.jpg" alt="" /><a href="#" class="return-home">Please
            return to homepage</a></figure>
      </div>
    </div>
  </div>
  @endsection