@extends('layout.master')

@section('title_name')
Home
@endsection

@section('body')
  <body class="product-page shop-checkout">
@endsection

@section('content')
      <section>
        <div class="banner-sub-page">
          <div class="container">
            <div class="breadcrumbs">
              <div class="breadcrumbs-list">
                <div class="page">Home</div>
                <div class="page">Checkout</div>
              </div>
            </div>
            <h2 class="sub-page-name">Checkout</h2>
          </div>
        </div>
      </section>
      <section>
        <div class="container">
          <div class="form-customers">
           

            <!--ERROR CODE-->
              @if (!$errors->isEmpty())
                <div class="bread-crumb radius" style="background: #b00303;">
                  <p style="margin:0 0 0 0;">You have incorrectly entered data. Please try again</p>
                </div>
              @endif

            @if(Auth::guard('member')->user() === null)
             <h3 class="check-out-title">Returning customers?</h3>
              <div class="woocommerce-checkout-info">Returning customer? 
                <div class="showlogin">Click here to login</div>
              </div>
            @endif
            <form class="login" role="form" method="POST" action="{{ url('/member/login') }}">
              {{ csrf_field() }}
              <p class="{{ $errors->has('email') ? ' has-error' : '' }}">
                <label>Username or email <span class="required">*</span></label>
                <input id="username" name="email" type="email" value="{{ old('email') }}" autofocus/>
                 @if ($errors->has('email'))
                     <span class="help-block">
                     <strong>{{ $errors->first('email') }}</strong>
                     </span>
                     @endif
              </p>
              <p class="{{ $errors->has('password') ? ' has-error' : '' }}">
                <label>Password   <span class="required">*</span></label>
                <input id="password" type="password" class="form-control" name="password"/>
                 @if ($errors->has('password'))
                     <span class="help-block">
                     <strong>{{ $errors->first('password') }}</strong>
                     </span>
                     @endif
              </p>
              <div class="clear"></div>
              <p>
                <input name="login" value="Login" type="submit" class="button"/>
                <label class="inline">
                  <input id="rememberme" value="forever" type="checkbox" name="remember"/> Remember me
                </label>
              </p>
              <div class="clear"></div>
            </form>
          </div>
          <form id="CheckoutForm" class="checkout" method="post" action="{{url('/checkout/payment')}}">
            <div id="customer_details" class="col2-set row">
              <div class="col-1 col-md-6">
                <div class="woocommerce-billing-fields">
                  <h3>Billing Details</h3>
                  <div class="row">
                    <div class="col-md-6">
                      <p>
                        <label>First Name <abbr title="required" class="required">*</abbr></label>
                        <input type="text"/>
                      </p>
                    </div>
                    <div class="col-md-6">
                      <p>
                        <label>Last Name <abbr title="required" class="required">*</abbr></label>
                        <input id="billing_last_name" name="billing_last_name" placeholder="" autocomplete="family-name" value="" type="text"/>
                      </p>
                    </div>
                  </div>
                  <p>
                    <label>Company Name</label>
                    <input type="text"/>
                  </p>
                  <div class="row">
                    <div class="col-md-6">
                      <p>
                        <label>Email Address <abbr title="required" class="required">*</abbr></label>
                        <input type="email"/>
                      </p>
                    </div>
                    <div class="col-md-6">
                      <p>
                        <label>Phone <abbr title="required" class="required">*</abbr></label>
                        <input type="tel"/>
                      </p>
                    </div>
                  </div>
                  <p>
                    <label>Country <abbr title="required" class="required">*</abbr></label>
                    <select id="billing_country" class="country_to_state country_select">
                      <option value="US">United States (US)</option>
                    </select>
                  </p>
                  <p>
                    <label>Address <abbr title="required" class="required">*</abbr></label>
                    <input type="text"/>
                  </p>
                  <p>
                    <input id="billing_address_2" name="billing_address_2" placeholder="Apartment, suite, unit etc. (optional)" autocomplete="address-line2" value="" type="text"/>
                  </p>
                  <p>
                    <label>State <abbr title="required" class="required">*</abbr></label>
                    <select class="country_to_state country_select" id="state" onchange="stateChose(this)" required>
                          <option value=""> - Chose State - </option>
                          @foreach($state as $x)  
                            <option value="{{$x['id']}}"> {{$x['name']}} </option>
                          @endforeach
                        </select>
                  </p>
                  <div class="row">
                    <div class="col-md-6">
                      <p>
                        <label>Town / City</label>
                        <select class="country_to_state country_select" name="city" id="city" onchange="cityChose(this)"  disabled="true" required>
                          <option value=""> - Chose State - </option>
                          @foreach($state as $x)  
                            <option value="{{$x['id']}}"> {{$x['name']}} </option>
                          @endforeach
                        </select>
                      </p>
                    </div>
                    <div class="col-md-6">
                      <p>
                        <label>Postcode / ZIP</label>
                        <input type="text"/>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-2 col-md-6">
                <div class="woocommerce-shipping-fields">
                  <h3>Additional Information</h3>
                  <p id="order_comments_field" class="notes woocommerce-validated">
                    <label>Order Notes</label>
                    <textarea id="order_comments" name="order_comments" placeholder="Notes about your order, e.g. special notes for delivery." rows="6" cols="5"></textarea>
                  </p>
                </div>
              </div>
            </div>
            <h3 id="order_review_heading">Your order</h3>
            <div id="order_review" class="woocommerce-checkout-review-order">
            @if (sizeof(Cart::content()) > 0)
              <table class="shop_table woocommerce-checkout-review-order-table">
                <thead>
                  <tr>
                    <th class="product-name">Product</th>
                    <th class="product-total">Total</th>
                  </tr>
                </thead>
                <tbody>
                @foreach (Cart::content() as $item)
                  <tr class="cart_item">
                    <td class="product-name">{{ $item->name }}<strong class="product-quantity"> × {{ $item->qty }}</strong></td>
                    <td class="product-total"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>{{ number_format(($item->price * $item->qty),0,",",".") }}</span></td>
                  </tr>
                @endforeach
                </tbody>
                <tfoot>
                  <tr class="cart-subtotal">
                    <th>Subtotal</th>
                    <td><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>{{number_format(Cart::subtotal(),0,",",".")}}</span></td>
                  </tr>
                  <tr class="order-total">
                    <th>Shipping</th>
                    <td><strong><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span id="shippingCost">0.00</span></strong></td>
                  </tr>
                  <tr class="order-total">
                    <th>Total</th>
                    <td><strong><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span id="totalPayment">0.00</span></strong></td>
                  </tr>
                </tfoot>
              </table>
            @endif
              <div id="payment" class="woocommerce-checkout-payment">
                <ul class="wc_payment_methods payment_methods methods">
                  <li class="payment_method_paypal">
                    <div class="payment_box_title">
                      <input id="payment_method_paypal" name="payment_method" value="paypal" data-order_button_text="Proceed to PayPal" type="radio" class="input-radio"/>
                      <label>PayPal <img src="/front_new/images/demo/pay.png" alt="PayPal Acceptance Mark"/><a href="#" title="What is PayPal?" class="about_paypal">What is PayPal?</a></label>
                    </div>
                  </li>
                </ul>
                <div class="place-order">
                  <noscript>Since your browser does not support JavaScript, or it is disabled, please ensure you click the <em>Update Totals</em> button before placing your order. You may be charged more than the amount stated above if you fail to do so.      <br/>
                  </noscript>
                  <input id="place_order" type="submit" class="button alt" value="PLACE ORDER"/>
                </div>
              </div>
            </div>
          </form>
        </div>
      </section>
    
    </div>
    <!-- .mv-site-->
    
@endsection
@section('custom_style')
<style type="text/css">
   .disabledbutton {
    pointer-events: none;
    opacity: 0.4;
}
.modal-backdrop {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 0;
    background-color: #000;
}
</style>
<link rel="stylesheet" href="/css/swal.css">
@endsection
@section('custom_script')
  <script src="{{asset('/back/plugins/jquery/jquery-1.9.1.min.js')}}"></script>
    @if(Auth::guard('member')->user() === null)
<script type="text/javascript">
   $( document ).ready(function() {
      $("#CheckoutForm").addClass("disabledbutton");
   });
</script>
@endif
 <script src="/js/swal.js"></script>
   @if (sizeof(Cart::content()) < 1)
  
   <script type="text/javascript">
      $( document ).ready(function() {
       swal("Sorry!", ", Your Cart is empty", "error");
       setTimeout(function () {
            window.location.href = "/"; //will redirect to your blog page (an ex: blog.html)
         }, 3000); //will call the function after 2 secs.
      });
   </script>
   
   @endif

  <script src="{{asset('/back/plugins/jquery/jquery-1.9.1.min.js')}}"></script>
  <script type="text/javascript">

   function stateChose(obj)
            {
              var state = document.getElementById('state');
              var state_id = state.options[state.selectedIndex].value;
         $.ajax({
              type: "POST",
              url: "/getcity",
              data: { "_token": "{{ csrf_token() }}",'state_id': state_id},
              success: function(data){
                 document.getElementById("city").disabled = false;
                  var route3 = data;
                  var inputTipe = $('#city');
                  var list = document.getElementById("city");
              var option ="";
                 while (list.hasChildNodes()) {
                    list.removeChild(list.firstChild);
                  }
                  var list = document.getElementById("city");
                  option += "<option value=''> - CHOSE CITY - </option>";
                    $.each(data,function(index,value){
                        option += "<option value='"+value['name']+"'>"+value['name']+"</option>";
                    });
              inputTipe.append(option);
                }
             });
                  
              
          }
   function cityChose(obj)
            {
              var city = document.getElementById('city');
              var city_name = city.options[city.selectedIndex].value;
         $.ajax({
              type: "POST",
              url: "/getcost",
              data: { "_token": "{{ csrf_token() }}",'city_name': city_name},
              success: function(data){
                 var x = data['cost'];
                 if(x === undefined){
                  x = 200;
                 }else{
                  x = x;
                 }
                 var total = document.getElementById('total_payment').value;
                  document.getElementById('shippingCost').innerHTML = "$ "+ x;
                  document.getElementById('nominal_payment').value = +x + +total;
                  document.getElementById('ongkir').value = x;
                  document.getElementById('totalPayment').innerHTML = +x + +total;
                }
             });
                  
              
          }
   
   
</script>
@endsection


