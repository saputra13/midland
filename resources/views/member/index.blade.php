@extends('member.layouts.master')

@section('title_name')
    Home
@endsection

@section('active_page')
    <li class="active">Dashboard</li>
@endsection

@section('content')
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li>@yield('active_page')</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Beranda</h1>
			<!-- end page-header -->
			<!-- begin row -->
			<div class="row">
			    <!-- begin col-3 -->
			    <div class="col-md-3 col-sm-6" style="color:#334152;">
			        <div class="widget widget-stats bg-white">
			            <div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>
			            <div class="stats-title" style="color:#334152;">PENDING</div>
			            <div class="stats-number" style="color:#334152;font-weight:bold;">{{$baru->count()}}</div>
			            <div class="stats-progress progress">
                            <div class="progress-bar" style="width: 0%;"></div>
                        </div>
			        </div>
			    </div>
			    <!-- end col-3 -->
			    <!-- begin col-3 -->
			    <div class="col-md-2 col-sm-6">
			        <div class="widget widget-stats bg-white">
			            <div class="stats-icon stats-icon-lg"><i class="fa fa-tags fa-fw"></i></div>
			            <div class="stats-title" style="color:#334152;">PAID</div>
			            <div class="stats-number" style="color:#334152;font-weight:bold;">{{$terbayar->count()}}</div>
			            <div class="stats-progress progress">
                            <div class="progress-bar"style="width: 0%;"></div>
                        </div>
			        </div>
			    </div>
				 <div class="col-md-2 col-sm-6">
			        <div class="widget widget-stats bg-white">
			            <div class="stats-icon stats-icon-lg"><i class="fa fa-tags fa-fw"></i></div>
			            <div class="stats-title" style="color:#334152;">PRODUCTION</div>
			            <div class="stats-number" style="color:#334152;font-weight:bold;">{{$produksi->count()}}</div>
			            <div class="stats-progress progress">
                            <div class="progress-bar"style="width: 0%;"></div>
                        </div>
			        </div>
			    </div>
			    <!-- end col-3 -->
			  	  <div class="col-md-3 col-sm-6">
			        <div class="widget widget-stats bg-white">
			            <div class="stats-icon stats-icon-lg"><i class="fa fa-shopping-cart fa-fw"></i></div>
			            <div class="stats-title" style="color:#334152;">DELIVERY</div>
			            <div class="stats-number" style="color:#334152;font-weight:bold;">{{$kirim->count()}}</div>
			            <div class="stats-progress progress">
                            <div class="progress-bar" style="width: 0%;"></div>
                        </div>
			        </div>
			    </div> 
				<!-- begin col-3 -->
			    <div class="col-md-2 col-sm-6">
			        <div class="widget widget-stats bg-white">
			            <div class="stats-icon stats-icon-lg"><i class="fa fa-shopping-cart fa-fw"></i></div>
			            <div class="stats-title" style="color:#334152;">ARRIVED</div>
			            <div class="stats-number" style="color:#334152;font-weight:bold;">{{$sampai->count()}}</div>
			            <div class="stats-progress progress">
                            <div class="progress-bar" style="width: 0%;"></div>
                        </div>
			        </div>
			    </div>
			    <!-- end col-3 -->
			    <!-- begin col-3 -->
			  
			    <!-- end col-3 -->
			</div>
			<!-- end row -->
		</div>
		<!-- end #content -->
@endsection	


	
@section('custom_script')	


	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="{{asset('/assets/plugins/chart-js/Chart.min.js')}}"></script>


	<!-- ================== END PAGE LEVEL JS ================== -->
@endsection

	
@section('custom_script_footer')

@endsection