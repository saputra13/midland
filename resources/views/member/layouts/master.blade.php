<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
  <meta name="description" content="<%= app.description %>">
  <meta name="keywords" content="<%= app.keywords %>">
  <meta name="author" content="<%= app.author %>">
  <title>{{ config('app.name') }} | @yield('title_name')</title>
  
  <!-- ================== BEGIN BASE CSS STYLE ================== -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
  <link href="{{asset('/back/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css')}}" rel="stylesheet" />
  <link href="{{asset('/back/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" />
  <link href="{{asset('/back/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" />
  <link href="{{asset('/back/css/animate.min.css')}}" rel="stylesheet" />
  <link href="{{asset('/back/css/style.min.css')}}" rel="stylesheet" />
  <link href="{{asset('/back/css/style-responsive.css')}}" rel="stylesheet" />
  <link href="{{asset('/back/css/theme/default.css')}}" rel="stylesheet" id="theme" />

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
  <!-- ================== END BASE CSS STYLE ================== -->
  <!-- ================== END BASE CSS STYLE ================== -->
  <style>
			.page-header {
				border-bottom: 1px solid rgba(0,0,0,.05);
			padding-bottom: 1rem;
			margin-bottom: 2rem;
			position: relative;
			z-index: 1;
			color: #3D5A80;
			font-weight: 600;
			font-size: 30px;
}
	.page-header::after {
		 content: "";
    background-color: #2f9cad;
    width: 22px;
    height: 6px;
    border-radius: 2px;
    display: block;
    position: absolute;
    bottom: -3px;
    left: 0;
	}
	</style>
  @yield('custom_style')
  
  <!-- ================== BEGIN BASE JS ================== -->
  <script src="{{asset('/back/plugins/pace/pace.min.js')}}"></script>
  <!-- ================== END BASE JS ================== -->
</head>
<body>
  <!-- begin #page-loader -->
  <div id="page-loader" class="fade in"><span class="spinner"></span></div>
  <!-- end #page-loader -->
  
  <!-- begin #page-container -->
  <div id="page-container" class="fade page-sidebar-fixed page-header-fixed">


  @include('member.layouts.header')  
    
    


    <!-- begin #sidebar -->

    @include('member.layouts.sidebar')


    <!-- end #sidebar -->

    <!-- begin #content -->
    @yield('content')
    <!-- end #content -->
    
    
    
    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->
      
  </div>
  <!-- end page container -->
  
 <!-- ================== BEGIN BASE JS ================== -->
  <script src="{{asset('/back/plugins/jquery/jquery-1.9.1.min.js')}}"></script>
  <script src="{{asset('/back/plugins/jquery/jquery-migrate-1.1.0.min.js')}}"></script>
  <script src="{{asset('/back/plugins/jquery-ui/ui/minified/jquery-ui.min.js')}}"></script>
  <script src="{{asset('/back/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
	
  <!--[if lt IE 9]>
    <script src="{{asset('/back/crossbrowserjs/html5shiv.js')}}"></script>
    <script src="{{asset('/back/crossbrowserjs/respond.min.js')}}"></script>
    <script src="{{asset('/back/crossbrowserjs/excanvas.min.js')}}"></script>
  <![endif]-->
  <script src="{{asset('/back/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
  <script src="{{asset('/back/plugins/jquery-cookie/jquery.cookie.js')}}"></script>
  <!-- ================== END BASE JS ================== -->
<script src="{{url('/back/plugins/bootstrap-slider/dist/js/bootstrap-slider.js')}}"></script>
  <!-- ================== BEGIN PAGE LEVEL JS ================== -->
  <script src="{{asset('/back/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
	<script src="{{asset('/back/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}"></script>
	<script src="{{asset('/back/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>
	<script src="{{asset('/back/plugins/masked-input/masked-input.min.js')}}"></script>
	<script src="{{asset('/back/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
	<script src="{{asset('/back/plugins/password-indicator/js/password-indicator.js')}}"></script>
	<script src="{{asset('/back/plugins/bootstrap-combobox/js/bootstrap-combobox.js')}}"></script>
	<script src="{{asset('/back/plugins/bootstrap-select/bootstrap-select.min.js')}}"></script>
	<script src="{{asset('/back/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>
	<script src="{{asset('/back/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js')}}"></script>
	<script src="{{asset('/back/plugins/jquery-tag-it/js/tag-it.min.js')}}"></script>
	<script src="{{asset('/back/plugins/bootstrap-daterangepicker/moment.js')}}"></script>
	<script src="{{asset('/back/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
	<script src="{{asset('/back/plugins/select2/dist/js/select2.min.js')}}"></script>
	<script src="{{asset('/back/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
	<script src="{{asset('/back/plugins/bootstrap-show-password/bootstrap-show-password.js')}}"></script>
	<script src="{{asset('/back/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js')}}"></script>
	<script src="{{asset('/back/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker.js')}}"></script>
	<script src="{{asset('/back/plugins/clipboard/clipboard.min.js')}}"></script>
	<script src="{{asset('/back/js/form-plugins.demo.min.js')}}"></script>
	<script src="{{asset('/back/js/apps.min.js')}}"></script>

  <!-- ================== END PAGE LEVEL JS ================== -->
  @yield('custom_script')
	@yield('custom_script_footer')

<script>
		$(document).ready(function() {
			App.init();
			FormPlugins.init();
		});		
</script>

</body>
</html>
