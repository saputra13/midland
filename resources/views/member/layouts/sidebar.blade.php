<!-- begin #sidebar -->
    <div id="sidebar" class="sidebar">
      <!-- begin sidebar scrollbar -->
      <div data-scrollbar="true" data-height="100%">
    <!-- begin sidebar user -->
    
    <ul class="nav">
      <li class="nav-profile">
        <div class="image">
          <a href="javascript:;"><img src="{{asset('images/user/default.jpg')}}" alt="" /></a>
        </div>
        <div class="info">
          <h4 style='color:#fff;'>{{ Auth::guard('member')->user()->email }}</h4>
        </div>
      </li>
    </ul>
    <!-- end sidebar user -->
    <!-- begin sidebar nav -->
    <ul class="nav" style='font-size:15px;color:#fff;'>
      <li class="has-sub" >
        <a href="{{ url('member/home') }}">
            <i class="fa fa-home"></i>
            <span>Beranda</span>
          </a>
      </li>
      <li class="has-sub" >
        <a href="{{ url('member/order') }}">
            <i class="fa fa-home"></i>
            <span>Order</span>
          </a>
      </li>
      
          <!-- begin sidebar minify button -->
      <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
          <!-- end sidebar minify button -->
    </ul>
    <!-- end sidebar nav -->
  </div>
  <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>