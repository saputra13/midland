@extends('member.layouts.master')

	@section('title_name')
	    Order
	@endsection

	@section('active_page')
	    <li class="active">Order</li>
	@endsection
	<style>

	 
			
		.pengiriman ul {
			list-style-type: none;
			margin: 0;
			padding: 0;
			overflow: hidden;
		}

		.pengiriman li {
			float: left;
			border: 1px solid #db285b;
			padding : 10px;
			font-size: 12px;
			line-height: 2;
			position: relative;
			list-style: none;
			border-radius: 100px;
			border: 1px solid #ddd;
			width: auto;
			height: 36px;
			display: inline-block;
			margin-bottom: 6px;
			padding: 6px 12px;
		}
		.pengiriman li::before  {
			    border: 1px solid #ddd;
				border-bottom: 0;
				width: 12px;
				content: " ";
				position: absolute;
				display: block;
				margin-left: -25px;
				top: 18px;
			
		}
		.pengiriman li:not(:first-child)  {
			    margin-left:12px;
		}

		 .pengiriman li a {
			display: block;
			color: white;
			text-align: center;
			padding: 16px;
			text-decoration: none;
		}

		.pengiriman li a:hover {
			background-color: #111111;
		}


	</style>

	@section('content')
			<!-- begin #content -->
			<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb hidden-print pull-right">
					<li><a href="javascript:;">Home</a></li>
					<li class="active">Order Detil</li>
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header hidden-print">Order</h1>
				<!-- end page-header -->
				
				<!-- begin invoice -->
				<div class="invoice">
	                <div class="invoice-company" style="color:#000;">
	                    <span class="pull-right hidden-print">
	                    </span>
	                    No Invoice : {{ $order['order_number'] }}  
	                    @if ($order['status'] === 'PENDING')
							<button class="btn btn-primary btn-sm">ORDER</button>
	                    @elseif ($order['status'] === 'PAID')
	                    	<button class="btn btn-success btn-sm">PAID</button>
						@elseif ($order['status'] === 'PRODUCTION')
							<button class="btn btn-warning btn-sm">PRODUCTION</button>
						@elseif ($order['status'] === 'DELIVERY')
							<button class="btn btn-pu$le btn-sm">DELIVERY</button>
						@elseif ($order['status'] === 'ARRIVED')
							<button class="btn btn-success btn-sm">ARRIVED</button>
						@endif
	                </div>
	                <div class="invoice-header">
	                    <div class="invoice-from">
	                        <small>Buyer</small>
	                        <address class="m-t-5 m-b-5">
	                            <strong>{{$order['member']['name']}}</strong><br />
	                            {{$order['member']['email']}}<br />
	                            {{$order['member']['phone']}}<br />
	                        </address>
	                    </div>
	                    <div class="invoice-to">
	                        <small>Alamat DELIVERY</small>
	                        <address class="m-t-8 m-b-8">
	                        		   <strong>{{$order['shipping']['name']}}</strong><br />
			                            {{$order['shipping']['address']['state']}} -  {{$order['shipping']['address']['city']}}<br />
			                            {{$order['shipping']['address']['address1']}}<br />
			                            {{$order['shipping']['address']['address2']}}<br />
			                            Phone: {{$order['shipping']['phone']}}<br />
			                            Email: {{$order['shipping']['email']}}<br />
			                            Business Address ? {{ $order['shipping']['business'] === '1' ? "YA" : "NO"}}<br />
	                        </address>
	                    </div>
	                    <div class="invoice-date" style="line-height: 20px;text-align:left;padding-left:80px;">
	                        <small>Order Detil</small>
	                        <div class="date m-t-5">{{$order['order_date']}}</div>
	                        <div class="invoice-detail">
	                            Dibayar Pada Tanggal : <label class='label label-primary'>{{$order['payment_date'] == '' ? "BELUM DIBAYAR" : $order['payment_date'] }}</label><br />
															@if (empty($order['payment_date']))
															<input type="hidden" name="status" value="PAID">
	                            <button type="button" class="btn btn-danger btn-sm">PENDING</button><br/>
	                            @else
															<input type="hidden" name="status" value="CANCEL">
	                            <button type="button" class="btn btn-success btn-sm">PAID</button>
	                            @endif
	                            
	                        </div>
	                    </div>
	                </div>
	                <div class="invoice-content">
	                    <div class="table-responsive">
	                        <table class="table table-invoice">
	                            <thead>
	                                <tr>
	                                    <th>PRODUCT</th>
	                                    <th>PRICE</th>
	                                    <th>QTY</th>
	                                    <th>TOTAL</th>
	                                </tr>
	                            </thead>
	                            <tbody>
	                                @foreach ($order['orderDetail'] as $x)
	                                <tr>
	                                    <td>{{$x->product->name}}</td>
	                                    <td>$ {{number_format($x->product->harga,0,".",",")}}</td>
	                                    <td>{{$x->qty}}</td>
	                                   <td>$ {{number_format($x->total,0,".",",")}}</td>
	                                </tr>
	                                @endforeach
	                            </tbody>
	                        </table>
	                    </div>
	                    <div class="invoice-price">
	                        <div class="invoice-price-left">
	                            <div class="invoice-price-row">
	                                <div class="sub-price">
	                                    <small>SUBTOTAL</small>
	                                  $ {{number_format($order['nominal_payment'],0,".",",")}}
	                                </div>
	                                <div class="sub-price">
	                                    <i class="fa fa-plus"></i>
	                                </div>
	                                <div class="sub-price">
	                                    <small>ONGKIR</small>
	                                    $ {{number_format($order['ongkir'],0,".",",")}}
	                                </div>
	                            </div>
	                        </div>
	                        <div class="invoice-price-right">
	                             <small>TOTAL</small>$ {{number_format(($order['nominal_payment'] + $order['ongkir']),0,".",",")}}
	                        </div>
	                    </div>
	                </div>
					<div class="invoice-header">
	                    <div class="col-md-12">
							<div class="col-md-6 pengiriman">
								<br/>
								<h3>Order Status</h3>
								@if ($order['status'] === 'ORDER')
								<ul>
									<li style="border:1px solid #D71149;"><em style="color:#D71149;" class="fa fa-shopping-cart"></em> <span style="color:#D71149;">Order</span></li>
									<li><em class="fa fa-money"></em>PAID</li>
									<li><em class="fa fa-home"></em> PRODUCTION</li>
									<li><em class="fa fa-inbox"></em> DELIVERY</li>
									<li><em class="fa fa-check-square-o"></em> ARRIVED</li>
								</ul>
								@elseif ($order['status'] === 'PAID')
								<ul>
									<li style="border:1px solid #D71149;"><em style="color:#D71149;" class="fa fa-shopping-cart"></em> <span style="color:#D71149;">Order</span></li>
									<li style="border:1px solid #D71149;"><em style="color:#D71149;" class="fa fa-money"></em> <span style="color:#D71149;">PAID</span></li>
									<li><em class="fa fa-home"></em> PRODUCTION</li>
									<li><em class="fa fa-inbox"></em> DELIVERY</li>
									<li><em class="fa fa-check-square-o"></em> ARRIVED</li>
								</ul>
								@elseif ($order['status'] === 'PRODUCTION')
								<ul>
									<li style="border:1px solid #D71149;"><em style="color:#D71149;" class="fa fa-shopping-cart"></em> <span style="color:#D71149;">Order</span></li>
									<li style="border:1px solid #D71149;"><em style="color:#D71149;" class="fa fa-money"></em> <span style="color:#D71149;">PAID</span></li>
									<li style="border:1px solid #D71149;"><em style="color:#D71149;" class="fa fa-home"></em> <span style="color:#D71149;">PRODUCTION</span></li>
									<li><em class="fa fa-inbox"></em> DELIVERY</li>
									<li><em class="fa fa-check-square-o"></em> ARRIVED</li>
								</ul>
								@elseif ($order['status'] === 'DELIVERY')
								<ul>
									<li style="border:1px solid #D71149;"><em style="color:#D71149;" class="fa fa-shopping-cart"></em> <span style="color:#D71149;">Order</span></li>
									<li style="border:1px solid #D71149;"><em style="color:#D71149;" class="fa fa-money"></em> <span style="color:#D71149;">PAID</span></li>
									<li style="border:1px solid #D71149;"><em style="color:#D71149;" class="fa fa-home"></em> <span style="color:#D71149;">PRODUCTION</span></li>
									<li style="border:1px solid #D71149;"><em style="color:#D71149;" class="fa fa-inbox"></em> <span style="color:#D71149;">DELIVERY</span></li>
									<li><em class="fa fa-check-square-o"></em> ARRIVED</li>
								</ul>
								@elseif ($order['status'] === 'ARRIVED')
								<ul>
									<li style="border:1px solid #D71149;"><em style="color:#D71149;" class="fa fa-shopping-cart"></em> <span style="color:#D71149;">Order</span></li>
									<li style="border:1px solid #D71149;"><em style="color:#D71149;" class="fa fa-money"></em> <span style="color:#D71149;">PAID</span></li>
									<li style="border:1px solid #D71149;"><em style="color:#D71149;" class="fa fa-home"></em> <span style="color:#D71149;">PRODUCTION</span></li>
									<li style="border:1px solid #D71149;"><em style="color:#D71149;" class="fa fa-inbox"></em> <span style="color:#D71149;">DELIVERY</span></li>
									<li style="border:1px solid #D71149;"><em style="color:#D71149;" class="fa fa-inbox"></em> <span style="color:#D71149;">ARRIVED</span></li>
								</ul>
								@endif
								
							</div>
							<div class="col-md-6">
								<br/>
								<h3>DELIVERY</h3>
								 <a href="/member/production-timeline/{{$order['id']}}" class="btn btn-lg btn-primary"><li class="fa fa-home"></li> PRODUCTION TIMELINE</a>
							</div>
						</div>
	                </div>
	                <div class="invoice-note">
	                    * Catatan Khusus<br />
	                </div>
	                <div class="invoice-footer text-muted">
	                    <p class="text-center m-b-5">
	                        midlandhomefurniture
	                    </p>
	                    <p class="text-center">
	                        <span class="m-r-10"><i class="fa fa-globe"></i> midlandhomefurniture.com</span>
	                        <span class="m-r-10"><i class="fa fa-phone"></i> T:016-18192302</span>
	                        <span class="m-r-10"><i class="fa fa-envelope"></i> system@midlandhomefurniture.com</span>
	                    </p>
	                </div>
	            </div>
				<!-- end invoice -->
			</div>
			<!-- end #content -->
		@endsection	
		
		<!-- ================== BEGIN BASE JS ================== -->
		<script src="assets/plugins/jquery/jquery-1.9.1.min.js"></script>
		<script src="assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
		<script src="assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
		<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<!--[if lt IE 9]>
			<script src="assets/crossbrowserjs/html5shiv.js"></script>
			<script src="assets/crossbrowserjs/respond.min.js"></script>
			<script src="assets/crossbrowserjs/excanvas.min.js"></script>
		<![endif]-->
		<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		<script src="assets/plugins/jquery-cookie/jquery.cookie.js"></script>
		<!-- ================== END BASE JS ================== -->
		
		<!-- ================== BEGIN PAGE LEVEL JS ================== -->
		<script src="assets/js/apps.min.js"></script>
		<!-- ================== END PAGE LEVEL JS ================== -->

