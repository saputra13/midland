@extends('member.layouts.master')

@section('title_name')
    Order
@endsection

@section('active_page')
    <li class="active">Order</li>
@endsection

@section('content')
		<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Beranda</a></li>
				<li><a href="javascript:;">Order</a></li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Order</h1>
			<!-- end page-header -->
			<!-- begin row -->
			<div class="row">
			    <!-- begin col-3 -->
			    <div class="col-md-3 col-sm-6" style="color:#334152;">
			        <div class="widget widget-stats bg-white">
			            <div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>
			            <div class="stats-title" style="color:#334152;">BELUM BAYAR</div>
			            <div class="stats-number" style="color:#334152;font-weight:bold;">{{$baru->count()}}</div>
			            <div class="stats-progress progress">
                            <div class="progress-bar" style="width: 0%;"></div>
                        </div>
			        </div>
			    </div>
			    <!-- end col-3 -->
			    <!-- begin col-3 -->
			    <div class="col-md-2 col-sm-6">
			        <div class="widget widget-stats bg-white">
			            <div class="stats-icon stats-icon-lg"><i class="fa fa-tags fa-fw"></i></div>
			            <div class="stats-title" style="color:#334152;">BELUM DI PRODUKSI</div>
			            <div class="stats-number" style="color:#334152;font-weight:bold;">{{$terbayar->count()}}</div>
			            <div class="stats-progress progress">
                            <div class="progress-bar"style="width: 0%;"></div>
                        </div>
			        </div>
			    </div>
				 <div class="col-md-2 col-sm-6">
			        <div class="widget widget-stats bg-white">
			            <div class="stats-icon stats-icon-lg"><i class="fa fa-tags fa-fw"></i></div>
			            <div class="stats-title" style="color:#334152;">BELUM DI PACK</div>
			            <div class="stats-number" style="color:#334152;font-weight:bold;">{{$produksi->count()}}</div>
			            <div class="stats-progress progress">
                            <div class="progress-bar"style="width: 0%;"></div>
                        </div>
			        </div>
			    </div>
			    <!-- end col-3 -->
			  	  <div class="col-md-3 col-sm-6">
			        <div class="widget widget-stats bg-white">
			            <div class="stats-icon stats-icon-lg"><i class="fa fa-shopping-cart fa-fw"></i></div>
			            <div class="stats-title" style="color:#334152;">ORDERAN DIJALAN</div>
			            <div class="stats-number" style="color:#334152;font-weight:bold;">{{$kirim->count()}}</div>
			            <div class="stats-progress progress">
                            <div class="progress-bar" style="width: 0%;"></div>
                        </div>
			        </div>
			    </div> 
				<!-- begin col-3 -->
			    <div class="col-md-2 col-sm-6">
			        <div class="widget widget-stats bg-white">
			            <div class="stats-icon stats-icon-lg"><i class="fa fa-shopping-cart fa-fw"></i></div>
			            <div class="stats-title" style="color:#334152;">ORDER SELESAI</div>
			            <div class="stats-number" style="color:#334152;font-weight:bold;">{{$sampai->count()}}</div>
			            <div class="stats-progress progress">
                            <div class="progress-bar" style="width: 0%;"></div>
                        </div>
			        </div>
			    </div>
			    <!-- end col-3 -->
			    <!-- begin col-3 -->
			  
			    <!-- end col-3 -->
			</div>
			<!-- begin row -->
			<div class="row">
					<div class="col-sm-6 mbtm-10">
						<form role="form" method="get" enctype="multipart/form-data" action="" class="form-inline mbtm-10" data-toggle="validator" novalidate="true">
							<div class="form-group">
								<select name="filter" style="width:150px" class="form-control" id="selectFilter">
									<option value="0">Nama Customer</option>
									<option value="1">No. PO</option>
									<option value="2">Nama Produk</option>
								</select>
							</div>
							<div class="form-group">
								<input style="width:150px;" class="form-control form-white" name="keyword" placeholder="Pencarian . . ." value="" type="text">
							</div>
							<div class="input-group">
								<button class="btn btn-default" id="btn_cari"><span class="glyphicon glyphicon-search"></span></button>
							</div>
						</form>
					</div>
					<div class="col-sm-6 mbtm-10">
						<div class="btn-group pull-right">
							<button class="btn btn-more btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
							MORE <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="#" id="downloadExcel"><span class="lnr lnr-download"></span> Download Excel</a></li>
							</ul>
						</div>
					</div>
			</div>
			<br/>
			<div class="row">
			   @foreach($order as $x)
					<div class="col-md-12">
			       <div class="panel panel-inverse">
								<div class="panel-body">
									<div class="data-order">
										<div class="col-md-3">
											<h4>{{$x['order_number']}}</h4>
											<br/>
											<label class="label label-primary">Buyer</label>
											<p style="font-weight:600;margin-top:5px;">{{$x['member']['name']}}</p>
											<label class="label label-primary">Order Date</label>
											<p style="font-weight:600;margin-top:5px;">{{$x['order_date']}}</p>
																					</div>
										<div class="col-md-3">
											<label class="label label-primary">PRODUCT</label>
											<br/>		
											<p style="font-weight:600;margin-top:5px;">
												@foreach($x['orderDetail'] as $a)
													{{$a['product']['name']}} X {{$a['qty']}}
												@endforeach
											</p>
											<hr/>
												@if($x['status'] == 'PENDING')
													<div id="stagihan4506727" class="alert alert-danger">
													<p>
														<small>PENDING ORDER</small>
														<br>
													</p>
													<p>
														<small>Sub Total : Rp. {{number_format($x['nominal_payment'],0,",",".")}}</small>
														<br>
													</p>
													<p>
														<small>Ongkir : Rp. {{number_format($x['ongkir'],0,",",".")}}</small>
														<br>
													</p>
													<p>
														<small>Please Pay your order</small>
														<br>
													</p>
														<p class=" mbtm-10" style="font-size:24px;">
															Rp. {{number_format(( $x['nominal_payment'] +  $x['ongkir']) ,0,",",".")}}
														</p>
																<br>
															<button data-toggle="modal" data-target="#modalinput"  class="btn btn-xs btn-success" id="idOrder" value="{{$x['id']}}" onclick="UpdateClick(this);"> Paid Confirmation</button>
														<p></p>
												</div>
												@elseif($x['status'] == 'KONFIRMASI')
												<div id="stagihan4506727" class="alert alert-warning">
													<p>
														<small>Your Order Konfirmasi</small>
														<br>
													</p>
													<p>
														<small>Sub Total : Rp. {{number_format($x['nominal_payment'],0,",",".")}}</small>
														<br>
													</p>
													<p>
														<small>Ongkir : Rp. {{number_format($x['ongkir'],0,",",".")}}</small>
														<br>
													</p>
													<p>
														<small>Your Order Konfirmasi</small>
														<br>
													</p>
														<p class=" mbtm-10" style="font-size:24px;">
																Rp. {{number_format(($x['nominal_payment'] + $x['ongkir'] ),0,",",".")}}
														</p>
																<br>
															<b>PROSES KONFIRMASI</b>
														<p></p>
												</div>
												@else
													<div id="stagihan4506727" class="alert alert-success">
													<p>
														<h5>Tagihan Sudah Terbayar</h5>
													</p>
													<p>
														<small>Sub Total : Rp. {{number_format($x['nominal_payment'],0,",",".")}}</small>
														<br>
													</p>
													<p>
														<small>Ongkir : Rp. {{number_format($x['ongkir'],0,",",".")}}</small>
														<br>
													</p>
														<p class=" mbtm-10" style="font-size:24px;">
																Rp. {{number_format(($x['nominal_payment'] + $x['ongkir'] ),0,",",".")}}
														</p>
																<br>
															<hr/>
															{{$x['payment_date']}} -  <span class="pull-right">PAYMENT METHOD <b>{{$x['payment_method']}}</b></span>
														<p></p>
													</div>
												@endif
										</div>
										<div class="col-md-3">
												<label class="label label-primary">Status Order</label>
											<div class="col-md-12 pengiriman">
													<br/>
													@if ($x['status'] === 'PENDING')
															<p>
																<a class="btn btn-success btn-icon btn-circle " title="ORDERRAN BARU"><i class="fa fa-shopping-cart"></i></a>
																<a class="btn btn-default btn-icon btn-circle " title="ORDERRAN TELAH TERBAYAR"><i class="fa fa-money"></i></a>
																<a class="btn btn-default btn-icon btn-circle " title="ORDERRAN SEDANG DI PRODUKSI"><i class="fa fa-home"></i></a>
																<a class="btn btn-default btn-icon btn-circle " title="ORDERRAN DALAM PENGIRIMAN"><i class="fa fa-inbox"></i></a>
																<a class="btn btn-default btn-icon btn-circle"  title="ORDERRAN SAMPAI" ><i class="fa fa-check-square-o"></i></a>
															 </i></a>
														</p>
														@elseif ($x['status'] === 'CONFIRMATION')
														<p>
																<a class="btn btn-success btn-icon btn-circle " title="PROSES KOFIRMASI"><i class="fa fa-shopping-cart"></i></a>
																<a class="btn btn-default btn-icon btn-circle " title="ORDERRAN TELAH TERBAYAR"><i class="fa fa-money"></i></a>
																<a class="btn btn-default btn-icon btn-circle " title="ORDERRAN SEDANG DI PRODUKSI"><i class="fa fa-home"></i></a>
																<a class="btn btn-default btn-icon btn-circle " title="ORDERRAN DALAM PENGIRIMAN"><i class="fa fa-inbox"></i></a>
																<a class="btn btn-default btn-icon btn-circle"  title="ORDERRAN SAMPAI" ><i class="fa fa-check-square-o"></i></a>
															 </i></a>
														</p>
														@elseif ($x['status'] === 'PAID')
														<p>
																<a class="btn btn-success btn-icon btn-circle " title="ORDERRAN BARU"><i class="fa fa-shopping-cart"></i></a>
																<a class="btn btn-success btn-icon btn-circle " title="ORDERRAN TELAH TERBAYAR"><i class="fa fa-money"></i></a>
																<a class="btn btn-default btn-icon btn-circle " title="ORDERRAN SEDANG DI PRODUKSI"><i class="fa fa-home"></i></a>
																<a class="btn btn-default btn-icon btn-circle " title="ORDERRAN DALAM PENGIRIMAN"><i class="fa fa-inbox"></i></a>
																<a class="btn btn-default btn-icon btn-circle"  title="ORDERRAN SAMPAI" ><i class="fa fa-check-square-o"></i></a>
															 </i></a>
														</p>
														@elseif ($x['status'] === 'PRODUCTION')
														<p>
																<a class="btn btn-success btn-icon btn-circle " title="ORDERRAN BARU"><i class="fa fa-shopping-cart"></i></a>
																<a class="btn btn-success btn-icon btn-circle " title="ORDERRAN TELAH TERBAYAR"><i class="fa fa-money"></i></a>
																<a class="btn btn-success btn-icon btn-circle " title="ORDERRAN SEDANG DI PRODUKSI"><i class="fa fa-home"></i></a>
																<a class="btn btn-default btn-icon btn-circle " title="ORDERRAN DALAM PENGIRIMAN"><i class="fa fa-inbox"></i></a>
																<a class="btn btn-default btn-icon btn-circle"  title="ORDERRAN SAMPAI" ><i class="fa fa-check-square-o"></i></a>
															 </i></a>
														</p>
														@elseif ($x['status'] === 'DELIVERY')
															<p>
																<a class="btn btn-success btn-icon btn-circle " title="ORDERRAN BARU"><i class="fa fa-shopping-cart"></i></a>
																<a class="btn btn-success btn-icon btn-circle " title="ORDERRAN TELAH TERBAYAR"><i class="fa fa-money"></i></a>
																<a class="btn btn-success btn-icon btn-circle " title="ORDERRAN SEDANG DI PRODUKSI"><i class="fa fa-home"></i></a>
																<a class="btn btn-success btn-icon btn-circle " title="ORDERRAN DALAM PENGIRIMAN"><i class="fa fa-inbox"></i></a>
																<a class="btn btn-default btn-icon btn-circle"  title="ORDERRAN SAMPAI" ><i class="fa fa-check-square-o"></i></a>
															 </i></a>
														</p>
														@elseif ($x['status'] === 'ARRIVED')
														<p>
																<a class="btn btn-success btn-icon btn-circle " title="ORDERRAN BARU"><i class="fa fa-shopping-cart"></i></a>
																<a class="btn btn-success btn-icon btn-circle " title="ORDERRAN TELAH TERBAYAR"><i class="fa fa-money"></i></a>
																<a class="btn btn-success btn-icon btn-circle " title="ORDERRAN SEDANG DI PRODUKSI"><i class="fa fa-home"></i></a>
																<a class="btn btn-success btn-icon btn-circle " title="ORDERRAN DALAM PENGIRIMAN"><i class="fa fa-inbox"></i></a>
																<a class="btn btn-success btn-icon btn-circle"  title="ORDERRAN SAMPAI" ><i class="fa fa-check-square-o"></i></a>
															 </i></a>
														</p>
														@endif
												
												
											</div><br>
											<div class="col-md-12 pengiriman">
													<label class="label label-primary">DELIVERY</label>
												<br/>
													<br/>
												<form action="{{ url('member/order/update', $x['id']) }}" method="post">
												{!! csrf_field() !!}
												@if ($x['status'] === 'DELIVERY')
												<input type="hidden" name="status" value="SAMPAI">
                                	            <button type="submit" class="btn btn-danger btn-sm">DELIVERED?</button><br/>
                                	            @elseif ($x['status'] === 'ARRIVED')
                                	            <a href="#" class="btn btn-success btn-sm">ARRIVED</a>
                                				@else
                                	            
                                				@endif
                                				</form>
											</div>
										</div>
										<div class="col-md-2">
											<label class="label label-warning">Notes</label>
												<div id="stagihan4506727" class="alert alert-primary">
													<p>
														Notes
													</p>
												</div>
										</div>	
										<div class="col-md-1">
											<p>
                       	<a href="{{url('member/order-detail',$x['id'])}}" class="btn btn-success btn-icon btn-circle"><i class="fa fa-list"></i></a>
											</p>
										</div>
									</div>
								</div>
						</div>
				</div>
				@endforeach
			</div>
			<div class="modal modal-danger fade" id="modalinput" tabindex="-1" role="dialog"
	     aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title">KONFIRMASI PEMBAYARAN</h4>
	            </div>
	            <div class="modal-body">
	                <input type="text" id="idUpdate">
	                <p style="font-weight: bold;">Apakah anda yakin sudah melakukan pembayaran?<br/>Jika anda yakin, silahkan lakukan kofirmasi. <br/>Admin kami akan memeriksa pembayaran paling lambat 24 Jam</p>
	                {{--<p>One fine body&hellip;</p>--}}
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-outline btn-flat pull-left" data-dismiss="modal">Batal</button>
	                {{--<button type="button" class="btn btn-outline">Save changes</button>--}}
	                <a href="#" id="yakinhapus" class="btn btn-outline btn-flat">Saya yakin</a>
	            </div>
	        </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->


@endsection			
@section('custom_style')
    <link href="assets/plugins/bootstrap-datepicker-master/bootstrap-datepicker3.css" rel="stylesheet">
			<style>
.btn-icon, .btn.btn-icon {
    display: inline-block;
    width: 28px;
    height: 28px;
      
    border: none;
        
    line-height: 28px;
    text-align: center;
	font-size: 14px;
				}
	 
			
		.pengiriman ul {
			list-style-type: none;
			margin: 0;
			padding: 0;
			overflow: hidden;
		}

		.pengiriman li {
			float: left;
			border: 2px solid #db285b;
			font-size: 12px;
			line-height: 2;
			position: relative;
			list-style: none;
			border-radius: 50%;
			width: 36px;
			height: 36px;
			display: inline-block;
			padding:6px 6px;
			margin-left:10px;
		}
		
		.pengiriman li:not(:first-child)  {
			    margin-left:9px;
		}
				.pengiriman em {
					font-size:20px;
				}

		.pengiriman li a:hover {
			background-color: #111111;
		}


	</style>
@endsection

@section('custom_script')
  <script>
  	function UpdateClick(btn) {
   	 $('#idUpdate').val(btn.value);
	}

$('#yakinhapus').click(function () {
    var token = $('#token').val();
    var id = $('#idUpdate').val();
    var route = "/member/order/payment/" + id;
    window.location = route;
});

  </script>
@endsection
