@extends('member.layouts.master')

@section('title_name')
    Setting
@endsection

@section('active_page')
    <li class="active">Setting</li>
@endsection
@section('content')
		<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li><a href="javascript:;">Setting</a></li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Setting</h1>
			<div class="row text-center">
        <div class="col-md-12">
				<div class="card">
						<div class="card-header">
							<ul class="nav nav-pills card-header-pills">
								<li class="nav-item">
									<a class="nav-link " href="{{url('/setting')}}">Profile</a>
								</li>
								<li class="nav-item">
									<a class="nav-link " href="{{url('/setting/avatar')}}">Avatar</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{url('/setting/password')}}">Password</a>
								</li>
								<li class="nav-item">
									<a class="nav-link active" href="{{url('/setting/bank')}}">Bank</a>
								</li>
							</ul>
						</div>
						</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-inverse" data-sortable-id="table-basic-7">
							<div class="panel-body">
									<div>
										<div class="col-md-11">
											<h2 class="page-header">Pengatuan Bank <br/><small>Masukkan data lengkap Anda </small></h2>
										</div>
										<div class="col-md-1">
											<li class="fa fa-money fa-4x" style="color:#2f9cad;"></li>
										</div>
									</div>
								<form action="{{url('/setting/bank/update')}}" method="POST">
										{!! csrf_field() !!}
                  <div class="col-md-12">
                      <fieldset>
                          <div class="form-group">
                              <label for="">Nama Bank</label>
                              <input type="text" class="form-control" id="" placeholder="" name="name" value="{{$member['bankmember']['name']}}" />
                          </div>
                           <div class="form-group">
                              <label for="">Nomor Rekening</label>
                              <input type="text" class="form-control" id="" placeholder="" name="no" value="{{$member['bankmember']['no']}}" />
                          </div>
                           <div class="form-group">
                              <label for="">Atas Nama</label>
                              <input type="text" class="form-control" id="" placeholder="" name="owner" value="{{$member['bankmember']['owner']}}" />
                          </div>
                           
                      </fieldset>
										<button type="submit" class="btn btn-lg btn-primary m-r-5"><li class="fa fa-save"></li> SIMPAN</button>
                  </div>
									
							</form>
							</div>
						</div>
			</div>
			</div>
				
			<!-- end row -->
		</div>
		<!-- end #content -->
		@endsection
       

@section('custom_style')	
			<link rel="stylesheet" href="{{url('admin/assets/plugins/simplemde/dist/simplemde.min.css')}}">
			<style>
	.editor-preview h1,
	.editor-preview h2,
	.editor-preview h3,
	.editor-preview h4,
	.editor-preview h5{
		margin-bottom:10px;
	}
	
	.editor-preview h1{
		border-bottom:1px solid #ddd;
	}
	
	.editor-preview h1{
		border-bottom:1px solid #eee;
	}
	</style>

			<style>
       .nav-pills > li > a.active, .nav-pills > li > a.active:focus, .nav-pills > li > a.active:hover {
        background: #FC4349;
        color: #fff;
      </style>
@endsection
			
@section('custom_script')	
		<script src="{{url('admin/assets/plugins/simplemde/dist/simplemde.min.js')}}"></script>
<script>
	new SimpleMDE({
		element: document.getElementById("demo1"),
		spellChecker: false,
	});
</script>
@endsection
	