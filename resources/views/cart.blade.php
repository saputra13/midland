@extends('layout.master')

@section('title_name')
Home
@endsection

@section('body')
  <body class="product-page cart">
@endsection

@section('content')
  
      <section>
        <div class="banner-sub-page">
          <div class="container">
            <div class="breadcrumbs">
              <div class="breadcrumbs-list">
                <div class="page">Home</div>
                <div class="page">Cart</div>
              </div>
            </div>
            <h2 class="sub-page-name">Cart</h2>
          </div>
        </div>
      </section>
      <section>
        <div class="container">
          <form class="cart-form">
            <table>
              <tr>
                <th>Product</th>
                <th>&#32;</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Total</th>
                <th>&#32;</th>
              </tr>
               @foreach (Cart::content() as $item)
              <tr>
                <td data-title="Product"><a href="#" class="image-product"><img src="/images/product/{{$item->options->images1}}" alt="tab-1" width="180" height="220"/></a></td>
                <td data-title="Name"><a href="#" class="name-product">{{ $item->name }}</a></td>
                <td data-title="Price"><span class="price">${{number_format($item->price,2,'.',',') }}   </span></td>
                <td data-title="Quantity"><span class="quanlity">Quantity:</span>
                  <input type="number"  class="form-control" value="{{$item->qty}}" itemid="{{$item->rowId}}" name="qty" id="qty" onchange="qtyChange(this)" min="0" />
                </td>
                <td data-title="Total"><span class="total">${{number_format(($item->price * $item->qty),0,',','.') }}</span></td>
              </tr>
              @endforeach

            </table>
            <div class="button-cart">
              <div class="button-cart-left">
                <input type="hidden" placeholder="Coupon code"/><a href="/catalog/" class="coupon">BACK TO SHOPPING</a>
              </div>
              <div class="button-cart-right">
                <a href="/checkout" class="process">Proceed to Checkout</a>
              </div>
            </div>
          </form>
          <div class="row">
            <div class="col-md-6">
              <form class="cart-total">
                <h4>Cart Totals</h4>
                <table>
                  <tr>
                    <td>Subtotal</td>
                    <td> <span class="subtotal">${{Cart::subtotal()}}</span></td>
                  </tr>
                  <tr>
                    <td>Total</td>
                    <td> <span class="total">${{Cart::total()}}</span></td>
                  </tr>
                </table>
              </form>
            </div>
          </div>
        </div>
      </section>
@endsection
@section('custom_script')
<script src="{{asset('assets/plugins/jquery/jquery-1.9.1.min.js')}}"></script>
<script type="text/javascript">
  function qtyChange(obj) {
    var qty = obj.value;
    var id = obj.getAttribute('itemid');
//    alert(id);
    $.ajax({
      type: "POST",
      url: "/cart/update",
      data: { "_token": "{{ csrf_token() }}","qty":qty,"id":id},
       success: function(html){ 
        // console.log(data);
         location.reload();
       }
     });
  }
  
</script>
@endsection