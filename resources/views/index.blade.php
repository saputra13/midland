@extends('layout.master')

@section('title_name')
Home
@endsection

@section('body')

<body class="home-1">
  @endsection

  @section('content')
  <div id="example-wrapper">
    <div class="section">
      <div class="slide-home slide-home-1">
        <div data-number="1" data-margin="100" data-loop="yes" data-navcontrol="yes" class="sofani-owl-carousel">

          @foreach($slider as $x)
          <div class="slide1">
            <div class="slide1-left">
              <div class="title"></div>
              <h1 style="color:#ffffff">{{$x['judul']}}</h1>
              <p style="color:#ffffff">{{$x['isi']}}</p>
            </div>
            <div class="slide1-right">
              <figure><img src="/images/slider/{{$x['slider']}}" alt="slide" height="555" style="width: 1920px;height: 555px;" /></figure>
              <!-- <div class="slide1-content" style="background: #ffffff;"><img src="/images/slider/{{$x['images']}}" alt="slide" style="margin-top:20px;max-width: 178px;max-height: 178px;"/></div> -->
            </div>
          </div>
          @endforeach

        </div>
      </div>
    </div>



    <section>
      <div class="product">
        <div class="sperator text-center">
          <p>OUR PRODUCTS</p>
          <div class="sperator-bottom"><img src="/front_new/images/demo/sperator.png" alt="spertor" /></div>
        </div>
        <div class="product-tab">
          <ul class="products-tabs nav nav-pills">
            <li class="active"><a href="#1a" data-toggle="tab">Chair</a></li>
            <li><a href="#2a" data-toggle="tab">Kitchen</a></li>
            <li><a href="#3a" data-toggle="tab">Sofa</a></li>
            <li><a href="#4a" data-toggle="tab">Consul</a></li>
            <li><a href="#5a" data-toggle="tab">Nightstand</a></li>
            <li><a href="#6a" data-toggle="tab">Table</a></li>
          </ul>
          <div class="desc-review-content tab-content clearfix">
            <!---LOOPING CATEGORY -->
            <div id="1a" class="tab-pane active">
              <div class="home-1-featured-products">
                <div class="products-in-category-tabs-wrapper">
                  <div class="products-content">
                    <div class="woocommerce product-listing columns-5 clearfix">
                      <!---LOOPING ITEM -->
                      @foreach($chair as $x)
                      <div class="product-item-wrap has-post-thumbnail">
                        <div class="product-item-inner">
                          <div class="product-thumb">
                            <div class="product-flash-wrap"></div>
                            <div class="product-thumb-primary"><img src="/images/product/{{$x['images1']}}" alt="39"
                                title="39" class="attachment-shop_catalog size-shop_catalog wp-post-image" style="width: 300px;height: 400px;" /></div>
                            <div class="product-thumb-secondary"><img src="/images/product/{{$x['images1']}}" alt="41"
                                class="attachment-shop_catalog size-shop_catalog" style="width: 300px;height: 400px;" /></div><a
                              href="/product/{{$x['id']}}" class="product-link">
                              <div class="product-hover-sign">
                                <hr />
                                <hr />
                              </div>
                            </a>
                            <div class="product-actions">
                              <div class="add-to-cart-wrap"><a href="#" class="add_to_cart_button"><i class="fa fa-cart-plus"></i>
                                  Add to cart</a></div>
                            </div>
                          </div>
                          <div class="product-info">
                            <div class="rate">Stock : {{$x['stock']}}</div><span class="price"><span class="woocommerce-Price-amount amount"><span
                                  class="woocommerce-Price-currencySymbol">$</span>{{$x['harga']}}</span></span><a href="#"
                              target="_blank">
                              <h3>{{ str_limit($x['name'],10) }}</h3>
                            </a>
                          </div>
                        </div>
                      </div>
                      @endforeach
                      <!---LOOPING ITEM -->

                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!---LOOPING CATEGORY -->
            <!---LOOPING CATEGORY -->
            <div id="2a" class="tab-pane active">
              <div class="home-1-featured-products">
                <div class="products-in-category-tabs-wrapper">
                  <div class="products-content">
                    <div class="woocommerce product-listing columns-5 clearfix">
                      <!---LOOPING ITEM -->
                      @foreach($kitchen as $x)
                      <div class="product-item-wrap has-post-thumbnail">
                        <div class="product-item-inner">
                          <div class="product-thumb">
                            <div class="product-flash-wrap"></div>
                            <div class="product-thumb-primary"><img src="/images/product/{{$x['images1']}}" alt="39"
                                title="39" class="attachment-shop_catalog size-shop_catalog wp-post-image" style="width: 300px;height: 400px;" /></div>
                            <div class="product-thumb-secondary"><img src="/images/product/{{$x['images1']}}" alt="41"
                                class="attachment-shop_catalog size-shop_catalog" style="width: 300px;height: 400px;" /></div><a
                              href="/product/{{$x['id']}}" class="product-link">
                              <div class="product-hover-sign">
                                <hr />
                                <hr />
                              </div>
                            </a>
                            <div class="product-actions">
                              <div class="add-to-cart-wrap"><a href="#" class="add_to_cart_button"><i class="fa fa-cart-plus"></i>
                                  Add to cart</a></div>
                            </div>
                          </div>
                          <div class="product-info">
                            <div class="rate">Stock : {{$x['stock']}}</div><span class="price"><span class="woocommerce-Price-amount amount"><span
                                  class="woocommerce-Price-currencySymbol">$</span>{{$x['harga']}}</span></span><a href="#"
                              target="_blank">
                              <h3>{{ str_limit($x['name'],10) }}</h3>
                            </a>
                          </div>
                        </div>
                      </div>
                      @endforeach
                      <!---LOOPING ITEM -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!---LOOPING CATEGORY -->
            <!---LOOPING CATEGORY -->
            <div id="3a" class="tab-pane ">
              <div class="home-1-featured-products">
                <div class="products-in-category-tabs-wrapper">
                  <div class="products-content">
                    <div class="woocommerce product-listing columns-5 clearfix">
                      <!---LOOPING ITEM -->
                      @foreach($sofa as $x)
                      <div class="product-item-wrap has-post-thumbnail">
                        <div class="product-item-inner">
                          <div class="product-thumb">
                            <div class="product-flash-wrap"></div>
                            <div class="product-thumb-primary"><img src="/images/product/{{$x['images1']}}" alt="39"
                                title="39" class="attachment-shop_catalog size-shop_catalog wp-post-image" style="width: 300px;height: 400px;" /></div>
                            <div class="product-thumb-secondary"><img src="/images/product/{{$x['images1']}}" alt="41"
                                class="attachment-shop_catalog size-shop_catalog" style="width: 300px;height: 400px;" /></div><a
                              href="/product/{{$x['id']}}" class="product-link">
                              <div class="product-hover-sign">
                                <hr />
                                <hr />
                              </div>
                            </a>
                            <div class="product-actions">
                              <div class="add-to-cart-wrap"><a href="#" class="add_to_cart_button"><i class="fa fa-cart-plus"></i>
                                  Add to cart</a></div>
                            </div>
                          </div>
                          <div class="product-info">
                            <div class="rate">Stock : {{$x['stock']}}</div><span class="price"><span class="woocommerce-Price-amount amount"><span
                                  class="woocommerce-Price-currencySymbol">$</span>{{$x['harga']}}</span></span><a href="#"
                              target="_blank">
                              <h3>{{ str_limit($x['name'],10) }}</h3>
                            </a>
                          </div>
                        </div>
                      </div>
                      @endforeach
                      <!---LOOPING ITEM -->

                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!---LOOPING CATEGORY -->
            <!---LOOPING CATEGORY -->
            <div id="4a" class="tab-pane ">
              <div class="home-1-featured-products">
                <div class="products-in-category-tabs-wrapper">
                  <div class="products-content">
                    <div class="woocommerce product-listing columns-5 clearfix">
                      <!---LOOPING ITEM -->
                      @foreach($consul as $x)
                      <div class="product-item-wrap has-post-thumbnail">
                        <div class="product-item-inner">
                          <div class="product-thumb">
                            <div class="product-flash-wrap"></div>
                            <div class="product-thumb-primary"><img src="/images/product/{{$x['images1']}}" alt="39"
                                title="39" class="attachment-shop_catalog size-shop_catalog wp-post-image" style="width: 300px;height: 400px;" /></div>
                            <div class="product-thumb-secondary"><img src="/images/product/{{$x['images1']}}" alt="41"
                                class="attachment-shop_catalog size-shop_catalog" style="width: 300px;height: 400px;" /></div><a
                              href="/product/{{$x['id']}}" class="product-link">
                              <div class="product-hover-sign">
                                <hr />
                                <hr />
                              </div>
                            </a>
                            <div class="product-actions">
                              <div class="add-to-cart-wrap"><a href="#" class="add_to_cart_button"><i class="fa fa-cart-plus"></i>
                                  Add to cart</a></div>
                            </div>
                          </div>
                          <div class="product-info">
                            <div class="rate">Stock : {{$x['stock']}}</div><span class="price"><span class="woocommerce-Price-amount amount"><span
                                  class="woocommerce-Price-currencySymbol">$</span>{{$x['harga']}}</span></span><a href="#"
                              target="_blank">
                              <h3>{{ str_limit($x['name'],10) }}</h3>
                            </a>
                          </div>
                        </div>
                      </div>
                      @endforeach
                      <!---LOOPING ITEM -->

                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!---LOOPING CATEGORY -->
            <!---LOOPING CATEGORY -->
            <div id="5a" class="tab-pane ">
              <div class="home-1-featured-products">
                <div class="products-in-category-tabs-wrapper">
                  <div class="products-content">
                    <div class="woocommerce product-listing columns-5 clearfix">
                      <!---LOOPING ITEM -->
                      @foreach($nakas as $x)
                      <div class="product-item-wrap has-post-thumbnail">
                        <div class="product-item-inner">
                          <div class="product-thumb">
                            <div class="product-flash-wrap"></div>
                            <div class="product-thumb-primary"><img src="/images/product/{{$x['images1']}}" alt="39"
                                title="39" class="attachment-shop_catalog size-shop_catalog wp-post-image" style="width: 300px;height: 400px;" /></div>
                            <div class="product-thumb-secondary"><img src="/images/product/{{$x['images1']}}" alt="41"
                                class="attachment-shop_catalog size-shop_catalog" style="width: 300px;height: 400px;" /></div><a
                              href="/product/{{$x['id']}}" class="product-link">
                              <div class="product-hover-sign">
                                <hr />
                                <hr />
                              </div>
                            </a>
                            <div class="product-actions">
                              <div class="add-to-cart-wrap"><a href="#" class="add_to_cart_button"><i class="fa fa-cart-plus"></i>
                                  Add to cart</a></div>
                            </div>
                          </div>
                          <div class="product-info">
                            <div class="rate">Stock : {{$x['stock']}}</div><span class="price"><span class="woocommerce-Price-amount amount"><span
                                  class="woocommerce-Price-currencySymbol">$</span>{{$x['harga']}}</span></span><a href="#"
                              target="_blank">
                              <h3>{{ str_limit($x['name'],10) }}</h3>
                            </a>
                          </div>
                        </div>
                      </div>
                      @endforeach
                      <!---LOOPING ITEM -->

                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!---LOOPING CATEGORY -->
            <!---LOOPING CATEGORY -->
            <div id="6a" class="tab-pane ">
              <div class="home-1-featured-products">
                <div class="products-in-category-tabs-wrapper">
                  <div class="products-content">
                    <div class="woocommerce product-listing columns-5 clearfix">
                      <!---LOOPING ITEM -->
                      @foreach($table as $x)
                      <div class="product-item-wrap has-post-thumbnail">
                        <div class="product-item-inner">
                          <div class="product-thumb">
                            <div class="product-flash-wrap"></div>
                            <div class="product-thumb-primary"><img src="/images/product/{{$x['images1']}}" alt="39"
                                title="39" class="attachment-shop_catalog size-shop_catalog wp-post-image" style="width: 300px;height: 400px;" /></div>
                            <div class="product-thumb-secondary"><img src="/images/product/{{$x['images1']}}" alt="41"
                                class="attachment-shop_catalog size-shop_catalog" style="width: 300px;height: 400px;" /></div><a
                              href="/product/{{$x['id']}}" class="product-link">
                              <div class="product-hover-sign">
                                <hr />
                                <hr />
                              </div>
                            </a>
                            <div class="product-actions">
                              <div class="add-to-cart-wrap"><a href="#" class="add_to_cart_button"><i class="fa fa-cart-plus"></i>
                                  Add to cart</a></div>
                            </div>
                          </div>
                          <div class="product-info">
                            <div class="rate">Stock : {{$x['stock']}}</div><span class="price"><span class="woocommerce-Price-amount amount"><span
                                  class="woocommerce-Price-currencySymbol">$</span>{{$x['harga']}}</span></span><a href="#"
                              target="_blank">
                              <h3>{{ str_limit($x['name'],10) }}</h3>
                            </a>
                          </div>
                        </div>
                      </div>
                      @endforeach
                      <!---LOOPING ITEM -->

                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!---LOOPING CATEGORY -->

          </div>
        </div>
      </div>
    </section>

    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="banner-page banner-page-left">
              <div class="banner-page-box">
                <figure><img src="/front_new/images/demo/banner.jpg" alt="banner" /></figure>
                <div class="content-hover">
                  <p>Use</p>
                  <p class="banner-name">Chair</p>
                </div>
                <div class="content-banner"><span>Midland Home Furniture</span>
                  <p class="banner-name bg-84deee">Chair</p>
                </div>
                <div class="cost">Shop</div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="banner-page banner-page-left">
              <div class="banner-page-box">
                <figure><img src="/front_new/images/demo/banner-h1-2.jpg" alt="banner" /></figure>
                <div class="content-hover">
                  <p>in the</p>
                  <p class="banner-name">living room</p>
                </div>
                <div class="content-banner"><span>We are sofani</span>
                  <p class="banner-name bg-9e7fe8">living room</p>
                </div>
                <div class="cost">Shop</div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="banner-page banner-page-left">
              <div class="banner-page-box">
                <figure><img src="/front_new/images/demo/banner-h1-3.jpg" alt="banner" /></figure>
                <div class="content-hover">
                  <p>in the</p>
                  <p class="banner-name">bath room</p>
                </div>
                <div class="content-banner"><span>We are sofani</span>
                  <p class="banner-name bg-81d742">bath room</p>
                </div>
                <div class="cost">Shop</div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="banner-page banner-page-left">
              <div class="banner-page-box">
                <figure><img src="/front_new/images/demo/banner-h1-4.jpg" alt="banner" /></figure>
                <div class="content-hover">
                  <p>in the</p>
                  <p class="banner-name">bed room</p>
                </div>
                <div class="content-banner"><span>We are sofani</span>
                  <p class="banner-name bg-ce78db">bed room</p>
                </div>
                <div class="cost">Shop</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <section>
      <div class="dales-this-week">
        <div class="sperator text-center">
          <p>NEWEST COLLECTIONS</p>
          <div class="sperator-bottom"><img src="/front_new/images/demo/sperator.png" alt="spertor" /></div>
        </div>
        <div class="home-1-deals-this-week yolo-wrap">
          <div class="vc_row wpb_row vc_row-fluid">
            <div class="container">
              <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner vc_custom_1461828027488">
                  <div class="wpb_wrapper">
                    <div id="shortcode-product-wrap-579a08bd562b2" class="shortcode-product-wrap">
                      <div class="product-wrap">
                        <div class="product-inner clearfix product-style-grid product-paging-none product-col-4">
                          <div class="woocommerce product-listing clearfix columns-4">
                            <!-- START LOOP-->
                            @foreach($new as $x)
                            <div class="product-item-wrap has-post-thumbnail">
                              <div class="product-item-inner">
                                <div class="product-thumb">
                                  <div class="product-flash-wrap"></div>
                                  <div class="product-thumb-primary"><img src="/images/product/{{$x['images1']}}" alt="39"
                                      title="39" class="attachment-shop_catalog size-shop_catalog wp-post-image" style="width: 300px;height: 340px;" /></div>
                                  <div class="product-thumb-secondary"><img src="/images/product/{{$x['images1']}}" alt="41"
                                      class="attachment-shop_catalog size-shop_catalog" style="width: 300px;height:340px;" /></div><a
                                    href="/product/{{$x['id']}}" class="product-link">
                                    <div class="product-hover-sign">
                                      <hr />
                                      <hr />
                                    </div>
                                  </a>
                                  <div class="product-actions">
                                    <div class="add-to-cart-wrap"><a href="#" class="add_to_cart_button"><i class="fa fa-cart-plus"></i>
                                        Add to cart</a></div>
                                  </div>
                                </div>
                                <div class="product-info">
                                  <div class="rate">Stock : {{$x['stock']}}</div><span class="price"><span class="woocommerce-Price-amount amount"><span
                                        class="woocommerce-Price-currencySymbol">$</span>{{$x['harga']}}</span></span><a
                                    href="#" target="_blank">
                                    <h3>{{ str_limit($x['name'],10) }}</h3>
                                  </a>
                                </div>
                              </div>
                            </div>
                            @endforeach
                            <!-- END LOOP-->
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- .home-1-deals-this-week-->
      </div>
      <!-- .home-1-deals-this-week-->
    </section>


    <div class="section-no-margin">
      <div class="icon-box-home-1">
        <div class="container">
          <div class="row">
            <div class="col-md-4">
              <div class="icon-box icon-box-style1">
                <div class="icon-box-left"><i class="fa fa-truck color-6fd9ec"></i></div>
                <div class="icon-box-right">
                  <p>FREE SHIPPING</p><span>
                    Duis sed odio sit amet nibh vulputate
                    a sit amet mauris.</span>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="icon-box icon-box-style1">
                <div class="icon-box-left"><i class="fa fa-tag color-ff9999"></i></div>
                <div class="icon-box-right">
                  <p>Price Promise</p><span>
                    Duis sed odio sit amet nibh vulputate
                    a sit amet mauris.</span>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="icon-box icon-box-style1">
                <div class="icon-box-left"><i class="fa fa-adjust color-ffdc73"></i></div>
                <div class="icon-box-right">
                  <p>1 Year Warranty</p><span>
                    Duis sed odio sit amet nibh vulputate
                    a sit amet mauris.</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection