@extends('layout.master')

@section('title_name')
Home
@endsection

@section('body')
  <body class="product-page product-grid product-3-columns-width-sidebar">
@endsection

@section('content')
      
      <section>
        <div class="home-1-featured-products">
          <div class="banner-sub-page">
            <div class="container">
              <div class="breadcrumbs">
                <div class="breadcrumbs-list">
                  <div class="page">Home</div>
                  <div class="page">Catalog</div>
                </div>
              </div>
              <h2 class="sub-page-name">Catalog</h2>
            </div>
          </div>
        </div>
      </section>
      <div class="section"> 
        <div class="products-in-category-tabs-wrapper container">
          <div class="row">
            <div class="col-md-3">
              <div class="sidebar-product">
                <aside class="categorie">
                  <h4>Product Categories</h4>
                  <ul class="categories">
                    @foreach($categories as $x)
                    <li>
                      <a href="/catalog/category/{{$x['name']}}">{{$x['name']}}</a>
                    </li>
                    @endforeach
                  </ul>
                </aside>
              </div>
            </div>
            <div class="col-md-9">
              <div class="products-content product-tab">
                <div class="woocommerce product-listing columns-3 clearfix">
                  <div class="desc-review-content tab-content clearfix">
                    <div id="1a" class="tab-pane active">
                          <!---LOOPING ITEM -->
                          @foreach($products  as $x)
                          <div class="product-item-wrap has-post-thumbnail">
                            <div class="product-item-inner">
                              <div class="product-thumb">
                                <div class="product-flash-wrap"></div>
                                  <div class="product-thumb-primary"><img  src="/images/product/{{$x['images1']}}" alt="39" title="39" class="attachment-shop_catalog size-shop_catalog wp-post-image" style="width: 280px;height: 300px;"/></div>
                                  <div class="product-thumb-secondary"><img  src="/images/product/{{$x['images1']}}" alt="41" class="attachment-shop_catalog size-shop_catalog" style="width: 300px;height: 400px;"/></div><a href="/product/{{$x['id']}}" class="product-link">
                                   <div class="product-hover-sign">
                                    <hr/>
                                    <hr/>
                                  </div></a>
                                <div class="product-actions">
                                  <div class="add-to-cart-wrap"><a href="#" class="add_to_cart_button"><i class="fa fa-cart-plus"></i> Add to cart</a></div>
                                </div>
                              </div>
                              <div class="product-info">
                                 <div class="rate">Stock : {{$x['stock']}}</div><span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>{{$x['harga']}}</span></span><a href="#" target="_blank">
                                  <h3>{{ str_limit($x['name'],10) }}</h3></a>
                              </div>
                            </div>
                          </div>
                          @endforeach
                           <!---LOOPING ITEM -->
                    </div>
                  </div>
                </div>
              </div>
              <div class="pagination-product text-right">
                <div class="pagination-list">
                  {{ $products->render()}}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    

    </div>
    <!-- .mv-site-->
   @endsection