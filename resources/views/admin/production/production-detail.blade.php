@extends('admin.layouts.master')

@section('title_name')
    PRODUKSI
@endsection

@section('active_page')
    <li class="active">Order</li>
@endsection

@section('content')
		<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb hidden-print pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li class="active">Detil Produksi</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header hidden-print">Detil Produksi</h1>
			<!-- end page-header -->
			
			<!-- begin invoice -->
			<div class="invoice">
                <div class="invoice-company" style="color:#000;">
                    <span class="pull-right hidden-print">
										  <form action="{{ url('admin/production-detail/update', $order['id']) }}" method="post">
															{!! csrf_field() !!}
															@if ($order['status'] === 'TERBAYAR')
															<input type="hidden" name="status" value="PRODUKSI">
	                            <button type="submit" class="btn btn-success btn-sm">PRODUKSI ORDERAN</button><br/>
	                            @elseif ($order['status'] === 'PRODUKSI')
															<input type="hidden" name="status" value="KIRIM">
	                            <button type="submit" class="btn btn-danger btn-sm">PRODUKSI SELESAI</button>
															@else
	                            	<a href="#" class="btn btn-success btn-sm">ORDERAN SUDAH DI PENGIRIMAN</a>
															@endif
															</form>
                    </span>
                    No Invoice : {{ $order['order_number'] }}     
											@if ($order['status'] === 'ORDER')
											<button class="btn btn-primary btn-sm">ORDER</button>
	                    @elseif ($order['status'] === 'TERBAYAR')
	                    <button class="btn btn-success btn-sm">TERBAYAR</button>
											@elseif ($order['status'] === 'PRODUKSI')
											<button class="btn btn-warning btn-sm">PRODUKSI</button>
											@elseif ($order['status'] === 'KIRIM')
											<button class="btn btn-success btn-sm">KIRIM</button>
											@elseif ($order['status'] === 'SAMPAI')
											<button class="btn btn-success btn-sm">SAMPAI</button>
											@endif
                </div>
                <div class="invoice-header">
	                    <div class="invoice-from">
	                        <small>member</small>
	                        <address class="m-t-5 m-b-5">
	                            <strong>{{$order['member']['name']}}</strong><br />
	                            {{$order['member']['email']}}<br />
	                            {{$order['member']['phone']}}<br />
	                        </address>
	                    </div>
	                    <div class="invoice-to">
	                        <small>Alamat Kirim</small>
	                        <address class="m-t-8 m-b-8">

	                         <strong>{{$order['shipping']['name']}}</strong><br />
			                            {{$order['shipping']['address']['province']}} -  {{$order['shipping']['address']['city']}}<br />
			                            {{$order['shipping']['address']['kecamatan']}}<br />
			                            {{$order['shipping']['address']['street']}}<br />
			                            Phone: {{$order['shipping']['phone']}}<br />
			                            Email: {{$order['shipping']['email']}}<br />
			                            Business Address ? {{ $order['shipping']['business'] === '1' ? "YA" : "NO"}}<br />
			                            Fax: (123) 456-7890
	                          
	                        </address>
	                    </div>
                <div class="invoice-content">
                    <div class="table-responsive">
                        <table class="table table-invoice">
                            <thead>
                                <tr>
                                    <th>SKU</th>
                                    <th>PRODUK</th>
                                    <th>JUMLAH</th>
                                    <th>DETAIL GAMBAR</th>
                                </tr>
                            </thead>
                            <tbody>
																@foreach ($order['orderDetail'] as $x)
																	 <tr>
																			<td>{{$x['product']['id']}}</td>
																			<td>{{$x['product']['name']}}</td>
																			<td>{{$x['qty']}}</td>
																			<td style="width:20%;"><img style="width:100%;" src="/images/product/{{$x['product']['images1']}}"></td>
																	</tr>
																@endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="invoice-note">
                    <h4>PROSES PRODUKSI</h4>
                    <a href="/admin/production-timeline/{{$order['id']}}" class="btn btn-lg btn-primary"><li class="fa fa-home"></li> LIHAT RIWAYAT PRODUKSI</a>
                </div>
                <div class="invoice-footer text-muted">
                    <p class="text-center m-b-5">
                        MOOTIVO
                    </p>
                    <p class="text-center">
                        <span class="m-r-10"><i class="fa fa-globe"></i> mootivo.com</span>
                        <span class="m-r-10"><i class="fa fa-phone"></i> T:016-18192302</span>
                        <span class="m-r-10"><i class="fa fa-envelope"></i> system@mootivo.com</span>
                    </p>
                </div>
            </div>
			<!-- end invoice -->
		</div>
		<!-- end #content -->
	@endsection	

@section('custom_script')
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="/assets/plugins/jquery/jquery-1.9.1.min.js"></script>
	<script src="/assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
	<script src="/assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
	<script src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!--[if lt IE 9]>
		<script src="assets/crossbrowserjs/html5shiv.js"></script>
		<script src="assets/crossbrowserjs/respond.min.js"></script>
		<script src="assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="/assets/js/admin/apps.min.js"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->
	@endsection
