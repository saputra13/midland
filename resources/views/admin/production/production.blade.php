@extends('admin.layouts.master')

@section('title_name')
    Production
@endsection

@section('active_page')
    <li class="active">Production</li>
@endsection

@section('content')
		<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Beranda</a></li>
				<li><a href="javascript:;">Produksi</a></li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Produksi</h1>
			<!-- end page-header -->
			<!-- begin row -->
			<div class="row">
			    <!-- begin col-3 -->
			    <div class="col-md-4 col-sm-6" style="color:#334152;">
			        <div class="widget widget-stats bg-white">
			            <div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>
			            <div class="stats-title" style="color:#334152;">TOTAL ORDER</div>
			            <div class="stats-number" style="color:#334152;font-weight:bold;">{{$terbayar->count()}}</div>
			            <div class="stats-progress progress">
                            <div class="progress-bar" style="width: 0%;"></div>
                        </div>
			        </div>
			    </div>
			    <!-- end col-3 -->
			    <!-- begin col-3 -->
			    <div class="col-md-4 col-sm-6">
			        <div class="widget widget-stats bg-white">
			            <div class="stats-icon stats-icon-lg"><i class="fa fa-tags fa-fw"></i></div>
			            <div class="stats-title" style="color:#334152;">TOTAL PRODUKSI</div>
			            <div class="stats-number" style="color:#334152;font-weight:bold;">{{$produksi->count()}}</div>
			            <div class="stats-progress progress">
                            <div class="progress-bar"style="width: 0%;"></div>
                        </div>
			        </div>
			    </div>
			    <!-- end col-3 -->
			    <!-- begin col-3 -->
			    <div class="col-md-4 col-sm-6">
			        <div class="widget widget-stats bg-white">
			            <div class="stats-icon stats-icon-lg"><i class="fa fa-shopping-cart fa-fw"></i></div>
			            <div class="stats-title" style="color:#334152;">TOTAL SELESAI</div>
			            <div class="stats-number" style="color:#334152;font-weight:bold;">{{$kirim->count()}}</div>
			            <div class="stats-progress progress">
                            <div class="progress-bar" style="width: 0%;"></div>
                        </div>
			        </div>
			    </div>
			    <!-- end col-3 -->
			    <!-- begin col-3 -->
			  
			    <!-- end col-3 -->
			</div>
			<!-- begin row -->
			<div class="row">
				<div class="col-md-12">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#default-tab-1" data-toggle="tab">
								<span class="visible-xs">Tab 1</span>
								<span class="hidden-xs"><b>ORDER TERBAYAR</b></span>
							</a>
						</li>
						<li class="">
							<a href="#default-tab-2" data-toggle="tab">
								<span class="visible-xs">Tab 2</span>
								<span class="hidden-xs"><b>PROSES PRODUKSI</b></span>
							</a>
						</li>
						<li class="">
							<a href="#default-tab-3" data-toggle="tab">
								<span class="visible-xs">Tab 3</span>
								<span class="hidden-xs"><b>SELESAI<b></span>
							</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane fade active in" id="default-tab-1">
							<div class="table-responsive">
								<table width="100%" id="dataTableBuilder" class="table table-striped table-hover">
									<thead>
										<tr>
											<th>NO INVOICE</th>
											<th>COSTUMER</th>
											<th>HP</th>
											<th>TANGGAL</th>
											<th>STATUS</th>
											<th>AKSI</th>
											<th>CETAK</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
						<div class="tab-pane fade" id="default-tab-2">
							<table width="100%" id="dataTableBuilder2" class="table table-striped table-hover">
								<thead>
									<tr>
										<th>NO INVOICE</th>
										<th>COSTUMER</th>
										<th>HP</th>
										<th>TANGGAL</th>
										<th>STATUS</th>
										<th>AKSI</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						<div class="tab-pane fade" id="default-tab-3">
							<table width="100%" id="dataTableBuilder3" class="table table-striped table-hover">
								<thead>
									<tr>
										<th>NO INVOICE</th>
										<th>COSTUMER</th>
										<th>HP</th>
										<th>TANGGAL</th>
										<th>STATUS</th>
										<th>AKSI</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- end row -->
		</div>
		<!-- end #content -->
	@endsection	

@section('custom_style')
        <!-- DataTables -->
   <link href="/back/plugins/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="/back/plugins/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <link href="/back/plugins/bootstrap-datepicker-master/bootstrap-datepicker3.css" rel="stylesheet">
@endsection

@section('custom_script')
    <script src="/back/plugins/DataTables/media/js/jquery.dataTables.js"></script>
    <script src="/back/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/Buttons/js/jszip.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/AutoFill/js/dataTables.autoFill.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/KeyTable/js/dataTables.keyTable.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/RowReorder/js/dataTables.rowReorder.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/Select/js/dataTables.select.min.js"></script>
    <script src="/back/js/table-manage-combine.demo.min.js"></script>
    <script src="/back/plugins/bootstrap-datepicker-master/bootstrap-datepicker.min.js"></script>
@endsection

@section('custom_script_footer')
    <script src="/json/page/production-order.js"></script>
    <script src="/json/page/production-wip.js"></script>
    <script src="/json/page/production-done.js"></script>
			
@endsection