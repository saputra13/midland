@extends('admin.layouts.master')

@section('title_name')
    PRODUKSI
@endsection

@section('active_page')
    <li class="active">Order</li>
@endsection
<style>

 
		
	.pengiriman ul {
		list-style-type: none;
		margin: 0;
		padding: 0;
		overflow: hidden;
	}

	.pengiriman li {
		float: left;
		border: 1px solid #db285b;
		padding : 10px;
		font-size: 12px;
		line-height: 2;
		position: relative;
		list-style: none;
		border-radius: 100px;
		border: 1px solid #ddd;
		width: auto;
		height: 36px;
		display: inline-block;
		margin-bottom: 6px;
		padding: 6px 12px;
	}
	.pengiriman li::before  {
		    border: 1px solid #ddd;
			border-bottom: 0;
			width: 12px;
			content: " ";
			position: absolute;
			display: block;
			margin-left: -25px;
			top: 18px;
		
	}
	.pengiriman li:not(:first-child)  {
		    margin-left:12px;
	}

	 .pengiriman li a {
		display: block;
		color: white;
		text-align: center;
		padding: 16px;
		text-decoration: none;
	}

	.pengiriman li a:hover {
		background-color: #111111;
	}


</style>

@section('content')
		<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Beranda</a></li>
				<li><a href="javascript:;">Production</a></li>
				<li class="active">Timeline</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Production Timeline </h1>
			<!-- end page-header -->
			
			<!-- begin timeline -->
			<div class="row">
				<div class="col-md-12">
	      			<div class="panel panel-inverse">
						<div class="panel-body">
							<h2 class="text-center">UPLOAD PROSES PRODUKSI</h2>
							<button title="Hapus Data" class="btn btn-primary btn-lg btn-block" data-toggle="modal" data-target="#modalinput" ><li class="fa fa-upload"></li> UPLOAD</button>
						</div>
					</div>
				</div>
			</div>
			<ul class="timeline">
			    @foreach($production as $x)
			    <li>
			        <div class="timeline-time">
			            <span class="date">
			            <h4>{{$x['date']}}</h4>
			        	</span>
			        </div>
			        <div class="timeline-icon">
			            <a href="javascript:;"><i class="fa fa-clock-o" style="padding: 10px;font-size: 20px;"></i></a>
			        </div>
			        <div class="timeline-body">
			            <div class="timeline-content">
                            <p>
                                {{$x['keterangan']}}
                            </p>
                            <img src="/images/production/{{$x['images']}}">
			            </div>
			        </div>
			    </li>
			    @endforeach
			</ul>
			<!-- end timeline -->
		</div>
		<div class="modal modal-danger fade" id="modalinput" tabindex="-1" role="dialog"
		     aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
		    <div class="modal-dialog" role="document">
		        <form action="{{url('/admin/production-timeline/upload', $order['id_order'])}}" method="post" enctype="multipart/form-data">
		        	{!! csrf_field() !!}
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                <h4 class="modal-title">UPLOAD PROSES PRODUKSI</h4>
		            </div>
		            <div class="modal-body">
		                <div class="form-group">
		                	<label class="control-label">Upload Foto Proses Produksi</label>
		                	<input type="file" name="images" class="form-control">
		                	<input type="hidden" name="id_order" class="form-control" value="{{$order['id']}}">
		            	</div>
		            	 <div class="form-group">
		                	<label class="control-label">Keterangan Proses Produksi</label>
		                	<textarea name="keterangan" class="form-control"></textarea>
		            	</div>
		                
		            </div>
		            <div class="modal-footer">
		                <button type="button" class="btn btn-outline btn-flat pull-left" data-dismiss="modal">Batal</button>
		                <button type="submit" class="btn btn-outline btn-success">Upload</button>
		            </div>
		        </div><!-- /.modal-content -->
		    	</form>
		    </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<!-- end #content -->
	@endsection	

@section('custom_script')
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="assets/plugins/jquery/jquery-1.9.1.min.js"></script>
	<script src="assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
	<script src="assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!--[if lt IE 9]>
		<script src="assets/crossbrowserjs/html5shiv.js"></script>
		<script src="assets/crossbrowserjs/respond.min.js"></script>
		<script src="assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/plugins/jquery-cookie/jquery.cookie.js"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="assets/js/apps.min.js"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->
	@endsection
