@extends('admin.layouts.master')

@section('title_name')
    Add New Category
@endsection

@section('active_page')
    <li class="active">Add New Category</li>
@endsection
	
@section('content')
		<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Beranda</a></li>
				<li><a href="{{ url('/category-type') }}">Add New Category</a></li>
					@yield('active_page')
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Add New Category</h1>
			<!-- end page-header -->
    @if (count($errors) > 0)
        <div class="alert alert-danger fade in m-b-15">
            <strong>Perhatian!</strong> Mohon isi data berikut.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
						<span class="close" data-dismiss="alert">&times;</span>
        </div>
    @endif			


	<!-- begin row -->
			<div class="row">
			    <div class="col-md-12">
			       <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                        <div class="panel-body">
							<div class="col-md-12">
								<form action="{{ url('/admin/category/add') }}" method="POST" class="form-horizontal" enctype="multipart/form-data" > 
									 {!! csrf_field() !!}
									<div class="form-group">
										<label class="col-md-3 control-label">Nama Category</label>
										<div class="col-md-9">
											<input type="text" class="form-control" name="name" placeholder="Category Name">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Images</label>
										<div class="col-md-9">
											<input type="file" class="form-control" name="images" value="">
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-9">
											<input type="submit" class="btn btn-primary" value="SIMPAN">
											<input type="reset" class="btn btn-danger" value="RESET">
										</div>
									</div>
								<form>
							</div>
						</div>
					</div>
			    </div>
			</div>
			<!-- end row -->
		</div>
		<!-- end #content -->
@endsection
						
@section('custom_style')
   
@endsection

@section('custom_script')
    
@endsection
				
@section('custom_script_footer')
@endsection