@extends('admin.layouts.master')

@section('title_name')
    Tambah Cluster
@endsection

@section('active_page')
    <li class="active">Tambah Cluster</li>
@endsection
	
@section('content')
		<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Beranda</a></li>
				<li><a href="{{ url('/category-type') }}">Cluster</a></li>
					@yield('active_page')
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Tambahkan Cluster</h1>
			<!-- end page-header -->
    @if (count($errors) > 0)
        <div class="alert alert-danger fade in m-b-15">
            <strong>Perhatian!</strong> Mohon isi data berikut.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
						<span class="close" data-dismiss="alert">&times;</span>
        </div>
    @endif			


	<!-- begin row -->
			<div class="row">
			    <div class="col-md-12">
			       <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                        <div class="panel-body">
							<div class="col-md-12">
								<form action="{{ url('admin/cluster/add') }}" method="POST" class="form-horizontal"> 
									 {!! csrf_field() !!}
									<div class="form-group">
										<label class="col-md-3 control-label">Nama Cluster</label>
										<div class="col-md-9">
											<input type="text" class="form-control" name="name" placeholder="Nama Cluster">
										</div>
									</div>
													
									<div class="form-group">
										<div class="col-md-9">
											<input type="submit" class="btn btn-primary" value="SIMPAN">
											<input type="reset" class="btn btn-danger" value="RESET">
										</div>
									</div>
								<form>
							</div>
						</div>
					</div>
			    </div>
			</div>
			<!-- end row -->
		</div>
		<!-- end #content -->
@endsection
				

@section('custom_script')
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="{{asset('/assets/plugins/jquery/jquery-1.9.1.min.js')}}"></script>
	<script src="{{asset('/assets/plugins/jquery/jquery-migrate-1.1.0.min.js')}}"></script>
	<script src="{{asset('/assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js')}}"></script>
	<script src="{{asset('/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
	<!--[if lt IE 9]>
		<script src="assets/crossbrowserjs/html5shiv.js"></script>
		<script src="assets/crossbrowserjs/respond.min.js"></script>
		<script src="assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="{{asset('/assets/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
	<script src="{{asset('/assets/plugins/jquery-cookie/jquery.cookie.js')}}"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
    <script src="{{asset('/assets/plugins/morris/raphael.min.js')}}"></script>
    <script src="{{asset('/assets/plugins/morris/morris.js')}}"></script>
    <script src="{{asset('/assets/plugins/jquery-jvectormap/jquery-jvectormap.min.js')}}"></script>
    <script src="{{asset('/assets/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js')}}"></script>
    <script src="{{asset('/assets/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js')}}"></script>
	<script src="{{asset('/assets/plugins/gritter/js/jquery.gritter.js')}}"></script>
	<script src="{{asset('/assets/js/dashboard-v2.min.js')}}"></script>
	<script src="{{asset('/assets/js/admin/apps.min.js')}}"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->
@endsection