@extends('admin.layouts.master')

@section('title_name')
    Product
@endsection

@section('active_page')
    <li class="active">Dashboard</li>
@endsection

@section('content')
        <!-- begin #content -->
        <div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb pull-right">
                <li><a href="javascript:;">Beranda</a></li>
                <li><a href="javascript:;">Admin</a></li>
                @yield('active_page')
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Beranda </h1>
            <!-- end page-header -->
            <!-- begin row -->
          
            <div class="row">
                <!-- begin col-3 -->
                <div class="col-md-4 col-sm-6">
                    <div class="widget widget-stats bg-green">
                        <div class="stats-icon"><i class="fa fa-shopping-cart"></i></div>
                        <div class="stats-info">
                            <h4>TOTAL KUNJUNGAN</h4>
                            <p></p>   
                        </div>
                        
                    </div>
                </div>
                <!-- end col-3 -->
                <!-- begin col-3 -->
                <div class="col-md-4 col-sm-6">
                    <div class="widget widget-stats bg-blue">
                        <div class="stats-icon"><i class="fa fa-credit-card"></i></div>
                        <div class="stats-info">
                            <h4>TOTAL PRODUCT</h4>
                            <p></p>   
                        </div>
                        
                    </div>
                </div>
                <!-- end col-3 -->
                <!-- begin col-3 -->
                <div class="col-md-4 col-sm-6">
                    <div class="widget widget-stats bg-purple">
                        <div class="stats-icon"><i class="fa fa-check-square-o"></i></div>
                        <div class="stats-info">
                            <h4>TOTAL KATEGORI</h4>
                            <p></p>   
                        </div>
                        
                    </div>
                </div>
               
                <!-- end col-3 -->
            </div>
            <!-- begin row -->
            <div class="row">
                <div class="col-md-12">
                   <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                        <div class="panel-body">
                           @include('admin.cluster.modal')
                            <table width="100%" id="dataTableBuilder" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="col-md-2">JUMLAH KUNJUNGAN</th>
                                        <th class="col-md-2">IMAGES PRODUK</th>
                                        <th class="col-md-2">NAMA PRODUK</th>
                                        <th class="col-md-2">HARGA</th>
                                        <th class="col-md-2">DISKON</th>
                                        <th>AKSI</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection 
        
@section('custom_style')
        <!-- DataTables -->
   <link href="/back/plugins/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="/back/plugins/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <link href="/back/plugins/bootstrap-datepicker-master/bootstrap-datepicker3.css" rel="stylesheet">
@endsection

@section('custom_script')
    <script src="/back/plugins/DataTables/media/js/jquery.dataTables.js"></script>
    <script src="/back/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/Buttons/js/jszip.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/AutoFill/js/dataTables.autoFill.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/KeyTable/js/dataTables.keyTable.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/RowReorder/js/dataTables.rowReorder.min.js"></script>
    <script src="/back/plugins/DataTables/extensions/Select/js/dataTables.select.min.js"></script>
    <script src="/back/js/admin//table-manage-combine.demo.min.js"></script>
    <script src="/back/plugins/bootstrap-datepicker-master/bootstrap-datepicker.min.js"></script>
@endsection

@section('custom_script_footer')
    <script src="/json/master/dashboard.js"></script>
@endsection