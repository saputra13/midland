@extends('admin.layouts.master')

@section('title_name')
    Edit Product
@endsection

@section('active_page')
    <li class="active">Edit Product</li>
@endsection
	
@section('content')
		<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Beranda</a></li>
				<li><a href="{{ url('admin/bank') }}">Product</a></li>
					@yield('active_page')
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Edit Product </h1>
			<!-- end page-header -->
    @if (count($errors) > 0)
        <div class="alert alert-danger fade in m-b-15">
            <strong>Perhatian!</strong> Mohon isi data berikut.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
						<span class="close" data-dismiss="alert">&times;</span>
        </div>
    @endif			


	<!-- begin row -->
			<div class="row">
			    <div class="col-md-12">
			       <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                        <div class="panel-body">
							<div class="col-md-12">
								<form action="{{ url('admin/product/update', $tampiledit->id) }}" method="POST" enctype="multipart/form-data" class="form-horizontal"> 
									 {!! csrf_field() !!}
									<div class="form-group">
										<label class="col-md-3 control-label">Nama Product</label>
										<div class="col-md-9">
											<input type="text" class="form-control" name="name" placeholder="Nama" value="{{ $tampiledit->name }}">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Finishing</label>
										<div class="col-md-9">
											<input type="text" class="form-control" name="finishing" placeholder="Finishing" value="{{ $tampiledit->finishing }}">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Material</label>
										<div class="col-md-9">
											<input type="text" class="form-control" name="material" placeholder="Material" value="{{ $tampiledit->material }}">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Harga</label>
										<div class="col-md-9">
											<input type="text" class="form-control" name="harga" placeholder="Harga" value="{{ $tampiledit->harga }}">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Discon</label>
										<div class="col-md-9">
											<input type="text" class="form-control" name="discon" placeholder="Discon" value="{{ $tampiledit->discon }}">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Spesifikasi</label>
										<div class="col-md-9">
											<textarea class="summernote" name="spesification">{{ $tampiledit->spesification }}</textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Deskripsi</label>
										<div class="col-md-9">
											<textarea class="form-control" name="description" placeholder="Deskipsi Product">{{ $tampiledit->description }}</textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Deskripsi Lengkap</label>
										<div class="col-md-9">
											<textarea class="summernote" name="long_des">{{ $tampiledit->long_des }} </textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Stock</label>
										<div class="col-md-9">
											<select name="stock" class="form-control" id="stock"> 
												<option value="READY" selected='{{ ($tampiledit->stock === "READY") ? "selected": "" }}'>READY</option>
												 <option value="PRE ORDER" selected='{{ ($tampiledit->stock === "PRE ORDER") ? "selected": "" }}' >PRE ORDER</option>
												 <option value="NO STOCK" selected='{{ ($tampiledit->stock === "NO STOCK") ? "selected": "" }}'>NO STOCK</option>
											</select>
										</div>
									</div>	
									<div class="form-group">
										<label class="col-md-3 control-label">Cluster</label>
										<div class="col-md-9">
											<select name="cluster" class="form-control" id="cluster"> 
												 @foreach($clusters as $cluster)
												    <option value="{{ $cluster->id }}" selected='{{ ($tampiledit->id_cluster === $cluster->id) ? "selected": "" }}'>{{ $cluster->name }}</option>
												  @endforeach
											</select>
										</div>
									</div>	
									<div class="form-group">
										<label class="col-md-3 control-label">Category</label>
										<div class="col-md-9">
											<select name="category" class="form-control" id="category"> 
												 @foreach($categorys as $x)
												    <option value="{{ $x->id }}" selected='{{ ($tampiledit->id_category === $x->id) ? "selected": "" }}'>{{ $x->name }}</option>
												  @endforeach
											</select>
										</div>
									</div>	
									<div class="form-group">
										<label class="col-md-3 control-label">GAMBAR 1</label>
										<div class="col-md-9">
											<input type="file"  name="foto1" class="validate"/ >
										</div>
									</div>	
									<div class="form-group">
										<label class="col-md-3 control-label">GAMBAR 2</label>
										<div class="col-md-9">
											<input type="file"  name="foto2" class="validate"/ >
										</div>
									</div>	
									<div class="form-group">
										<label class="col-md-3 control-label">GAMBAR 3</label>
										<div class="col-md-9">
											<input type="file"  name="foto3" class="validate"/ >
										</div>
									</div>	
									<div class="form-group">
										<label class="col-md-3 control-label">GAMBAR 4</label>
										<div class="col-md-9">
											<input type="file"  name="foto4" class="validate"/ >
										</div>
									</div>	
									<div class="form-group">
										<label class="col-md-3 control-label"></label>
										<div class="col-md-9">
											<img src="{{ asset('images/product/'.$tampiledit->images1) }}" id="showgambar" style="max-width:200px;max-height:200px;float:left;" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label"></label>
										<div class="col-md-9">
											<img src="{{ asset('images/product/'.$tampiledit->images2) }}" id="showgambar" style="max-width:200px;max-height:200px;float:left;" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label"></label>
										<div class="col-md-9">
											<img src="{{ asset('images/product/'.$tampiledit->images3) }}" id="showgambar" style="max-width:200px;max-height:200px;float:left;" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label"></label>
										<div class="col-md-9">
											<img src="{{ asset('images/product/'.$tampiledit->images4) }}" id="showgambar" style="max-width:200px;max-height:200px;float:left;" />
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-9">
											<input type="submit" class="btn btn-primary" value="SIMPAN">
											<input type="button" class="btn btn-danger" value="KEMBALI" onclick="window.location.href='{{ url('admin/product') }}'">
										</div>
									</div>
								<form>
							</div>
						</div>
					</div>
			    </div>
			</div>
			<!-- end row -->
		</div>
		<!-- end #content -->
@endsection
				
 @section('custom_style')
	<link href="{{asset('/assets/plugins/summernote/summernote.css')}}" rel="stylesheet" />
@endsection  			

@section('custom_script')
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="{{asset('/assets/plugins/jquery/jquery-1.9.1.min.js')}}"></script>
	<script src="{{asset('/assets/plugins/jquery/jquery-migrate-1.1.0.min.js')}}"></script>
	<script src="{{asset('/assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js')}}"></script>
	<script src="{{asset('/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
	<!--[if lt IE 9]>
		<script src="assets/crossbrowserjs/html5shiv.js"></script>
		<script src="assets/crossbrowserjs/respond.min.js"></script>
		<script src="assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="{{asset('/assets/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
	<script src="{{asset('/assets/plugins/jquery-cookie/jquery.cookie.js')}}"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="{{asset('/assets/plugins/gritter/js/jquery.gritter.js')}}"></script>
	<script src="{{asset('/assets/js/admin/dashboard-v2.min.js')}}"></script>
	<script src="{{asset('/assets/js/admin/apps.min.js')}}"></script>

	<script src="{{asset('/assets/plugins/summernote/summernote.min.js')}}"></script>
	<script src="{{asset('/assets/js/admin/form-summernote.demo.min.js')}}"></script>
	<script src="{{asset('/assets/js/admin/apps.min.js')}}"></script>
	<script>
		$(document).ready(function() {
			App.init();
			FormSummernote.init();
		});
	</script>

	<!-- ================== END PAGE LEVEL JS ================== -->
@endsection
				
@section('custom_script_footer')

@endsection
