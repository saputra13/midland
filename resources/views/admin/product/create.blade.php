@extends('admin.layouts.master')

@section('title_name')
    Tambah Product
@endsection

@section('active_page')
    <li class="active">Tambah Product</li>
@endsection
	
@section('content')
		<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Beranda</a></li>
				<li><a href="{{ url('bank') }}">Product</a></li>
					@yield('active_page')
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Tambah Product </h1>
			<!-- end page-header -->
    @if (count($errors) > 0)
        <div class="alert alert-danger fade in m-b-15">
            <strong>Perhatian!</strong> Mohon isi data berikut.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
						<span class="close" data-dismiss="alert">&times;</span>
        </div>
    @endif			


	<!-- begin row -->
			<div class="row">
			    <div class="col-md-12">
			       <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                        <div class="panel-body">
							<div class="col-md-12">
								<form action="{{ url('/admin/product/add') }}" method="POST" enctype="multipart/form-data" class="form-horizontal"> 
									 {!! csrf_field() !!}
									<div class="form-group">
										<label class="col-md-3 control-label">Nama Product</label>
										<div class="col-md-9">
											<input type="text" class="form-control" name="name" placeholder="Nama">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Finishing</label>
										<div class="col-md-9">
											<input type="text" class="form-control" name="finishing" placeholder="Finishing">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Material</label>
										<div class="col-md-9">
											<input type="text" class="form-control" name="material" placeholder="Material">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Harga</label>
										<div class="col-md-9">
											<input type="text" class="form-control" name="harga" placeholder="Harga">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Discon</label>
										<div class="col-md-9">
											<input type="text" class="form-control" name="discon" placeholder="Discon">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Spesifikasi</label>
										<div class="col-md-9">
											<textarea class="summernote" name="spesification"></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Deskripsi</label>
										<div class="col-md-9">
											<textarea class="form-control" name="description" placeholder="Deskipsi Product"></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Deskripsi Lengkap</label>
										<div class="col-md-9">
											<textarea class="summernote" name="long_des"></textarea>
										</div>
									</div>
										<div class="form-group">
										<label class="col-md-3 control-label">Stock</label>
										<div class="col-md-9">
											<select name="stock" class="form-control" id="stock"> 
												
												    <option value="READY" >READY</option>
												 <option value="PRE ORDER" >PRE ORDER</option>
												 <option value="NO STOCK" >NO STOCK</option>
											</select>
										</div>
									</div>	
									<div class="form-group">
										<label class="col-md-3 control-label">Cluster</label>
										<div class="col-md-9">
											<select name="cluster" class="form-control" id="cluster"> 
												 @foreach($clusters as $cluster)
												    <option value="{{ $cluster->id }}" >{{ $cluster->name }}</option>
												  @endforeach
											</select>
										</div>
									</div>	
									<div class="form-group">
										<label class="col-md-3 control-label">Category</label>
										<div class="col-md-9">
											<select name="category" class="form-control" id="category"> 
												 @foreach($categorys as $category)
												    <option value="{{ $category->id }}" >{{ $category->name }}</option>
												  @endforeach
											</select>
										</div>
									</div>	
									<div class="form-group">
										<label class="col-md-3 control-label">GAMBAR 1</label>
										<div class="col-md-9">
											<input type="file"  name="foto1" class="validate"/ >
										</div>
									</div>	
									<div class="form-group">
										<label class="col-md-3 control-label">GAMBAR 2</label>
										<div class="col-md-9">
											<input type="file"  name="foto2" class="validate"/ >
										</div>
									</div>	
									<div class="form-group">
										<label class="col-md-3 control-label">GAMBAR 3</label>
										<div class="col-md-9">
											<input type="file"  name="foto3" class="validate"/ >
										</div>
									</div>	
									<div class="form-group">
										<label class="col-md-3 control-label">GAMBAR 4</label>
										<div class="col-md-9">
											<input type="file"  name="foto4" class="validate"/ >
										</div>
									</div>	
									
									<div class="form-group">
										<div class="col-md-9">
											<input type="submit" class="btn btn-primary" value="SIMPAN">
											<input type="reset" class="btn btn-danger" value="RESET">
										</div>
									</div>
								<form>
							</div>
						</div>
					</div>
			    </div>
			</div>
			<!-- end row -->
		</div>
		<!-- end #content -->
@endsection
@section('custom_style')
	<link href="{{url('back/plugins/summernote/summernote.css')}}" rel="stylesheet" />
@endsection  			

@section('custom_script')
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="{{url('back/plugins/jquery/jquery-1.9.1.min.js')}}"></script>
	<script src="{{url('back/plugins/jquery/jquery-migrate-1.1.0.min.js')}}"></script>
	<script src="{{url('back/plugins/jquery-ui/ui/minified/jquery-ui.min.js')}}"></script>
	<script src="{{url('back/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
	<!--[if lt IE 9]>
		<script src="crossbrowserjs/html5shiv.js"></script>
		<script src="crossbrowserjs/respond.min.js"></script>
		<script src="crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="{{url('back/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
	<script src="{{url('back/plugins/jquery-cookie/jquery.cookie.js')}}"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="{{url('back/plugins/gritter/js/jquery.gritter.js')}}"></script>
	<script src="{{url('back/js/dashboard-v2.min.js')}}"></script>
	<script src="{{url('back/js/apps.min.js')}}"></script>

	<script src="{{url('back/plugins/summernote/summernote.min.js')}}"></script>
	<script src="{{url('back/js/form-summernote.demo.min.js')}}"></script>
	<script src="{{url('back/js/apps.min.js')}}"></script>
	<script>
		$(document).ready(function() {
			App.init();
			FormSummernote.init();
		});
	</script>

	<!-- ================== END PAGE LEVEL JS ================== -->
@endsection
				
@section('custom_script_footer')

@endsection

