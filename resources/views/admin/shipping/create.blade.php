@extends('admin.layouts.master')
	@section('title_name')
	Add New Shipping Cost
	@endsection
	@section('active_page')
	<li class="active">Add New Shipping Cost</li>
	@endsection
@section('content')
<!-- begin #content -->
<div id="content" class="content">
   <!-- begin breadcrumb -->
   <ol class="breadcrumb pull-right">
      <li><a href="javascript:;">Beranda</a></li>
      <li><a href="{{ url('/category-type') }}">Shipping</a></li>
      @yield('active_page')
   </ol>
   <!-- end breadcrumb -->
   <!-- begin page-header -->
   <h1 class="page-header">Add New Shipping Cost</h1>
   <!-- end page-header -->
   @if (count($errors) > 0)
   <div class="alert alert-danger fade in m-b-15">
      <strong>Perhatian!</strong> Mohon isi data berikut.<br><br>
      <ul>
         @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
         @endforeach
      </ul>
      <span class="close" data-dismiss="alert">&times;</span>
   </div>
   @endif			
   <!-- begin row -->
   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-inverse" data-sortable-id="table-basic-7">
            <div class="panel-body">
               <div class="col-md-12">
                  <form action="{{ url('admin/shipping/add') }}" method="POST" class="form-horizontal">
                  {!! csrf_field() !!}
                  <div class="form-group">
                     <label class="col-md-3 control-label">State</label>
                     <div class="col-md-9">
                        <select name="state" class="form-control" id="state" onchange="stateChose(this)">
                           @foreach($state as $x)
                           <option value="{{$x['id']}}">{{$x['name']}}</option>
                           @endforeach
                        </select>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-md-3 control-label">City</label>
                     <div class="col-md-9">
                         <select name="city" class="form-control" id="city" disabled="true">
                           <option value=""> - CHOSE CITY - </option>

                        </select>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-md-3 control-label">Cost ($)</label>
                     <div class="col-md-9">
                        <input type="text" class="form-control" name="cost" placeholder="">
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="col-md-9">
                        <input type="submit" class="btn btn-primary" value="SIMPAN">
                        <input type="reset" class="btn btn-danger" value="RESET">
                     </div>
                  </div>
                  <form>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- end row -->
</div>
<!-- end #content -->
@endsection
@section('custom_script_footer')
  <script src="{{asset('/back/plugins/jquery/jquery-1.9.1.min.js')}}"></script>
  <script type="text/javascript">

   function stateChose(obj)
            {
              var state = document.getElementById('state');
              var state_id = state.options[state.selectedIndex].value;
   		$.ajax({
              type: "POST",
              url: "/admin/getcity",
              data: { "_token": "{{ csrf_token() }}",'state_id': state_id},
              success: function(data){
   				  document.getElementById("city").disabled = false;
                  var route3 = data;
                  var inputTipe = $('#city');
                  var list = document.getElementById("city");
				  var option ="";
   				  while (list.hasChildNodes()) {
                    list.removeChild(list.firstChild);
                  }
                  var list = document.getElementById("cost");
   					option += "<option value=''> - SILAHKAN PILIH JENIS SERVICE EKSPEDISI - </option>";
                    $.each(data,function(index,value){
                     	option += "<option value='"+value['name']+"'>"+value['name']+"</option>";
                    });
				  inputTipe.append(option);
                }
             });
   					
              
          }
   
   
</script>
@endsection

