@extends('layout.master')

@section('title_name')
Profile
@endsection

@section('body')

<body class="about-us">
    @endsection

    @section('content')
    <div id="example-wrapper">
        <div class="section">
            <div class="banner-sub-page">
                <div class="container">
                    <div class="breadcrumbs">
                        <div class="breadcrumbs-list">
                            <div class="page">Home</div>
                            <div class="page">Profile</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div class="about-us-content">
                            <h2>APTA</h2>
                            <div class="wpb_text_column wpb_content_element">
                                <div class="wpb_wrapper">
                                    <h3>Apta Is an Furniture & Interior
                                        consultant Based in
                                        Jepara Central Java Indonesia.
                                    </h3>
                                </div>
                            </div>
                            <div class="wpb_single_image wpb_content_element vc_align_center">
                                <figure class="wpb_wrapper vc_figure">
                                    <div class="vc_single_image-wrapper vc_box_border_grey"><img src="/images/bg.jpg"
                                            alt="img 8" class="vc_single_image-img attachment-full" width="920" height="350"></div>
                                </figure>
                            </div>
                            <div class="wpb_text_column wpb_content_element">
                                <div class="wpb_wrapper">
                                    <p>
                                        is specialized in the production of classical furniture entirely made in Jepara
                                        an artisan tradition that has its highest points in the classic furniture in
                                        Baroque style, Louis style, but also in classic Indonesia furnishings..
                                        The classic furniture are rigorously made in Jepara Indonesia by artisan
                                        production, with carved parts, finishings in best color and inlaid all
                                        handmade.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="about-bottom">
                                    <h2>Vision</h2>
                                    <div class="row">
                                        <div class="about-bottom-left">
                                            <div class="wpb_wrapper">
                                                <ul>
                                                    <li>Become a professional furniture company that has appropriate
                                                        management in managing
                                                        the organization and running a business.</li>

                                                    <li>Become a trusted company that has high accountability and
                                                        credibility.</li>

                                                    <li>Become a chosen furniture company that has the highest priority
                                                        in collaborating with all
                                                        interested clients</li>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="about-bottom">
                                    <h2>Mision</h2>
                                    <div class="row">
                                        <div class="about-bottom-left">
                                            <div class="wpb_wrapper">
                                                <ul>
                                                    <li>Develop products and markets for customer satisfaction.</li>
                                                    <li>Building company partnerships to increase supplier trust</li>
                                                    <li>Increase added value for the environment and surrounding
                                                        communities</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_single_image wpb_content_element vc_align_center">
                                <figure class="wpb_wrapper vc_figure">
                                    <div class="vc_single_image-wrapper vc_box_border_grey"><img src="/images/team.jpg"
                                            alt="img 8" class="vc_single_image-img attachment-full" width="920" height="350"></div>
                                </figure>
                            </div>
                            <div class="wpb_single_image wpb_content_element vc_align_center">
                                <figure class="wpb_wrapper vc_figure">
                                    <div class="vc_single_image-wrapper vc_box_border_grey"><img src="/images/structure.png"
                                            alt="img 8" class="vc_single_image-img attachment-full" width="920" height="350"></div>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @endsection