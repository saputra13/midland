

@extends('layout.master')
@section('title_name')
Home
@endsection
@section('body')
<body class="product-page single-product">
   <body class="home-1">
      @endsection
      @section('content')
      <section>
         <div class="banner-sub-page">
            <div class="container">
               <div class="breadcrumbs">
                  <div class="breadcrumbs-list">
                     <div class="page">Shop </div>
                     <div class="page">Essential</div>
                     <div class="page">Baxter</div>
                     <div class="page">Cleansing Bar</div>
                  </div>
               </div>
               <h2 class="sub-page-name">{{$product['category']['name']}}</h2>
            </div>
         </div>
      </section>
      <section class="product-information">
         <div class="container">
            <div class="row">
               <div class="col-md-6">
                  <div id="sync1" class="owl-carousel owl-template">
                     <div class="item">
                        <figure><img src="/images/product/{{$product['images1']}}" alt="slide" width="540" height="700"/></figure>
                     </div>
                     <div class="item">
                        <figure><img src="/images/product/{{$product['images1']}}" alt="slide" width="540" height="700"/></figure>
                     </div>
                     <div class="item">
                        <figure><img src="/images/product/{{$product['images1']}}" alt="slide" width="540" height="700"/></figure>
                     </div>
                     <div class="item">
                        <figure><img src="/images/product/{{$product['images1']}}" alt="slide" width="540" height="700"/></figure>
                     </div>
                  </div>
                  <div id="sync2" class="owl-carousel owl-template">
                     <div class="item">
                        <figure><img src="/images/product/{{$product['images1']}}" alt="slide" width="180" height="220"/></figure>
                     </div>
                     <div class="item">
                        <figure><img src="/images/product/{{$product['images1']}}" alt="slide" width="180" height="220"/></figure>
                     </div>
                     <div class="item">
                        <figure><img src="/images/product/{{$product['images1']}}" alt="slide" width="180" height="220"/></figure>
                     </div>
                     <div class="item">
                        <figure><img src="/images/product/{{$product['images1']}}" alt="slide" width="180" height="220"/></figure>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="summary-product entry-summary">
                     <h1 class="product_title">{{$product['name']}}</h1>
                     <div class="woocommerce-product-rating"></div>
                     <div class="rate-price">
                        <p class="price"><span>${{number_format($product['harga'],2,'.',',')}}</span></p>
                     </div>
                     <div class="product-single-short-description">
                        <p>{!! $product['spesification'] !!}</p>
                     </div>
                     <form method="post" action="{{url('cart/add')}}" enctype="multipart/form-data" class="cart">
                        {!! csrf_field() !!}
                        <div class="quantity">
                           <div class="quantity-inner">
                              <input type="hidden" name="id" value="{{ $product['id'] }}">
                              <input type="hidden" name="name" value="{{ $product['name'] }}">
                              <input type="hidden" name="sale_price" value="{{ $product['harga'] }}">
                              <input step="1" min="1" name="qty" value="1" title="Qty" size="4" type="number" class="input-text qty text"/>
                           </div>
                        </div>
                        <button type="submit" class="single_add_to_cart_button button alt">Add to cart</button>
                     </form>
                     <div class="product_meta"><span class="sku_wrapper">
                        <label>Availability:</label><span class="product-stock-status in-stock">{{ $product['stock'] }}</span></span><span class="posted_in">
                        <label>Category:</label><a href="#">{{ $product['category']['name'] }}</a>.</span>
                     </div>
                     <label>Cluster:</label><span class="sku">{{ $product['cluster']['name'] }}</span>.</span><span class="product-stock-status-wrapper">
                     <label>Material:</label><a href="#">{{ $product['material'] }}</a>.</span>
                  </div>
                  <label>Finishing:</label><a href="#">{{ $product['finishing'] }}</a>.</span>
               </div>
            </div>
         </div>
      </section>
      <section>
         <div class="container">
            <div class="product-single-tab">
               <div class="row">
                  <div class="col-md-2"></div>
                  <div class="col-md-8">
                     <ul class="nav nav-pills">
                        <li class="active"><a href="#1a" data-toggle="tab">Description</a></li>
                     </ul>
                     <div class="desc-review-content tab-content clearfix">
                        <div id="1a" class="tab-pane active">
                           <p>{!!$product['long_des']!!}</p>
                        </div>
                       
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="relative-single-page">
         <div class="container">
            <h2>Related Products</h2>
            <div class="related-logo">
               <div data-number="4" data-margin="10" data-loop="yes" data-navcontrol="yes" class="sofani-owl-carousel">
                  @foreach($products as $x)
                    <div class="product-item-wrap has-post-thumbnail">
                            <div class="product-item-inner">
                              <div class="product-thumb">
                                <div class="product-flash-wrap"></div>
                                  <div class="product-thumb-primary"><img  src="/images/product/{{$x['images1']}}" alt="39" title="39" class="attachment-shop_catalog size-shop_catalog wp-post-image" style="width: 300px;height: 320px;"/></div>
                                  <div class="product-thumb-secondary"><img  src="/images/product/{{$x['images1']}}" alt="41" class="attachment-shop_catalog size-shop_catalog" style="width: 300px;height: 320px;"/></div><a href="/product/{{$x['id']}}" class="product-link">
                                   <div class="product-hover-sign">
                                    <hr/>
                                    <hr/>
                                  </div></a>
                                <div class="product-actions">
                                  <div class="add-to-cart-wrap"><a href="#" class="add_to_cart_button"><i class="fa fa-cart-plus"></i> Add to cart</a></div>
                                </div>
                              </div>
                              <div class="product-info">
                                 <div class="rate">Stock : {{$x['stock']}}</div><span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>{{$x['harga']}}</span></span><a href="#" target="_blank">
                                  <h3>{{ str_limit($x['name'],10) }}</h3></a>
                              </div>
                            </div>
                          </div>
                  @endforeach
               </div>
            </div>
         </div>
      </section>
      <!-- .mv-site-->
      @endsection

