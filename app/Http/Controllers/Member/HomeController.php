<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\Address;
use App\Model\Design;
use App\Model\Mockup;
use App\Model\Buyer;
use App\Model\Campaign;
use App\Member;
use App\Model\Agent;
use App\Model\Profit;
use Auth;
use Carbon\Carbon;
use Analytics;
use Spatie\Analytics\Period;
use App\Libraries\GoogleAnalytics;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{

    public function index()
    {
      $userid =  Auth::guard('member')->user()->id;
      $member = Member::where('id',$userid)->first();
      $member_id = $member['id'];

      $order = Order::where('id_member',$member_id)->get();
      $baru = Order::where('status','PENDING')->where('id_member',$member_id)->get();
      $terbayar = Order::where('status','PAID')->where('id_member',$member_id)->get();
      $produksi = Order::where('status','PRODUCTION')->where('id_member',$member_id)->get(); 
      $kirim = Order::where('status','DELIVERY')->where('id_member',$member_id)->get(); 
      $sampai = Order::where('status','ARRIVED')->where('id_member',$member_id)->get(); 

        return view('member.index',compact('order'))
        	->with('baru',$baru)
        	->with('terbayar',$terbayar)
        	->with('produksi',$produksi)
        	->with('kirim',$kirim)
        	->with('sampai',$sampai);
    }
}
