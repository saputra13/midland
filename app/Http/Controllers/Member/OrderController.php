<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\Address;
use App\Model\Design;
use App\Model\Mockup;
use App\Model\Buyer;
use App\Model\Campaign;
use App\Member;
use App\Model\Agent;
use App\Model\Profit;
use Auth;
use Carbon\Carbon;
use Analytics;
use Spatie\Analytics\Period;
use App\Libraries\GoogleAnalytics;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function index()
    {
      
      $userid =  Auth::guard('member')->user()->id;
      $member = Member::where('id',$userid)->first();
      $member_id = $member['id'];

      $order = Order::where('id_member',$member_id)->get();
      $baru = Order::where('status','ORDER')->where('id_member',$member_id)->get();
      $terbayar = Order::where('status','TERBAYAR')->where('id_member',$member_id)->get();
      $produksi = Order::where('status','PRODUKSI')->where('id_member',$member_id)->get(); 
      $kirim = Order::where('status','KIRIM')->where('id_member',$member_id)->get(); 
      $sampai = Order::where('status','SAMPAI')->where('id_member',$member_id)->get(); 

        return view('member.order.order',compact('order'))->with('baru',$baru)->with('terbayar',$terbayar)->with('produksi',$produksi)->with('kirim',$kirim)->with('sampai',$sampai);
    }
    public function orderdetail(Request $request, $id)
    {
        $order = Order::findorFail($id);
        return view('member.order.order-detail',compact('order'));
    }
    public function update(Request $request, $id){
        $status = $request['status'];
        $order = Order::where('id',$id)->first();
        $order->status = $status;
        $order->update();
        
        return back(); 
      
    }
     public function timeline(Request $request, $id){
      $production = Production::where('id_order',$id)->get();
      $order = Order::findorFail($id);
      return view('member.order.production-timeline',compact('production','order'));
    }
  
    public function show(){
      
    }

    public function payment($id){
      $order = Order::where('id',$id)->first();
      $order->status = "KONFIRMASI";
      $order->update();

      return back();
    }
}