<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\Address;
use App\Model\Design;
use App\Model\Mockup;
use App\Model\Buyer;
use App\Model\Campaign;
use App\Model\Profit;
use App\Member;
use Ramsey\Uuid\Uuid;
use App\Model\DetailProfit;
use App\Model\ProfitPerusahaan;
use App\Model\ModelProduct;
  
use Carbon\Carbon;
use Response;
use PDF;


class OrderController extends Controller
{
    public function index()
    {
      $order = Order::All();
      $baru = Order::where('status','ORDER')->get();
      $terbayar = Order::where('status','TERBAYAR')->get();
      $produksi = Order::where('status','PRODUKSI')->get(); 
      $kirim = Order::where('status','KIRIM')->get(); 
      $sampai = Order::where('status','SAMPAI')->get(); 

        return view('admin.order.order',compact('order'))->with('baru',$baru)->with('terbayar',$terbayar)->with('produksi',$produksi)->with('kirim',$kirim)->with('sampai',$sampai);
    }
    public function getdataorder() {
        $order = order::all();
        $cacah = 0;
        $data = [];

        foreach ($order as $i => $d) {
            $data[$cacah] = [
              $d->order_number,
              $d->buyer->firstname,
              $d->buyer->phone,
              $d->order_date,
              $d->status,
              $d->id 
            ];

            $cacah++;    
        }

        return response()->json([
            'data' => $data
        ]);
    }
     public function orderdetail(Request $request, $id)
    {
        $order = Order::with(['shipping','shipping.address','member'])->findorFail($id);
        return view('admin.order.order-detail',compact('order'));
    }
    public function update(Request $request, $id){
        $status = $request['status'];
        $order = Order::where('id',$id)->first();
        $order->payment_date = Carbon::now();
        $order->status = $status;
        $order->update();
        
        return back(); 
      
    }
   public function delete($id){
      $order = Order::findorFail($id);
      $order->delete();
      return back();
    }
  
    public function show(){
      
    }
}