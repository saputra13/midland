<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Slider;
use Ramsey\Uuid\Uuid;
use Yajra\Datatables\Datatables;
use Response;


class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.slider.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin.slider.create');
    }


    public function getdataslider() {
            $Slider = Slider::all();
            $cacah = 0;
            $data = [];

            foreach ($Slider as $i => $d) {
                $data[$cacah] = [
                  $d->slider,
                  $d->judul,
                  $d->isi,
                  $d->id 
                ];

                $cacah++;    
            }

            return response()->json([
                'data' => $data
            ]);
        }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request, Slider $slider)
    {
      $this->validate($request, [
            'judul' => 'required',
            'isi' => 'required',

      ]);

           $tambah = new slider();
           $tambah->id = Uuid::uuid4();
           $tambah->judul = $request['judul'];
           $tambah->isi = $request['isi'];  

          $upload_path ='images/slider';
           if( $request->file('images') != ""){
             //1 
             $images       = $request->file('images');
             $ext1        = $images->getClientOriginalExtension();
             $fileName1   = "images-".date('YmdHis')."1.$ext1";
             $request->file('images')->move($upload_path, $fileName1);
             $tambah->images = $fileName1;
           }
          if( $request->file('slider') != ""){
           //2 
           $slider       = $request->file('slider');
           $ext2        = $slider->getClientOriginalExtension();
           $fileName2   = "slider-".date('YmdHis')."2.$ext2";
           $request->file('slider')->move($upload_path, $fileName2);
           $tambah->slider = $fileName2;
          }
      
           $tambah->save();

           return redirect()->to('/admin/slider')
                            ->with('success','Item updated successfully');

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tampiledit = Slider::where('id', $id)->first();
        return view('admin.slider.edit')->with('tampiledit', $tampiledit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function update(Request $request, $id)
    {
           $update =  slider::where('id',$id)->first();
           $update->judul = $request['judul'];
           $update->isi = $request['isi'];  

          $upload_path ='images/slider';
           if( $request->file('images') != ""){
             //1 
             $images       = $request->file('images');
             $ext1        = $images->getClientOriginalExtension();
             $fileName1   = "images-".date('YmdHis')."1.$ext1";
             $request->file('images')->move($upload_path, $fileName1);
             $update->images = $fileName1;
           }
          if( $request->file('slider') != ""){
           //2 
           $slider       = $request->file('slider');
           $ext2        = $slider->getClientOriginalExtension();
           $fileName2   = "slider-".date('YmdHis')."2.$ext2";
           $request->file('slider')->move($upload_path, $fileName2);
           $update->slider = $fileName2;
          }
      
         
        $update->update();

        return redirect()->to('/admin/slider');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hapus = Slider::find($id);
        $hapus->delete();

        return redirect()->to('/admin/slider');
    }
}
