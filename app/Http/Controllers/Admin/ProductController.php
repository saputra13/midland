<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\Cluster;
use App\Model\Product;
use Ramsey\Uuid\Uuid;
use Yajra\Datatables\Datatables;
use Response;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.product.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $clusters = Cluster::All();
     $categorys = Category::All();
      return view('admin.product.create')->with('clusters',$clusters)->with('categorys',$categorys);
    }


    public function getdataproduct() {
            $Product = Product::with('category')->with('cluster')->get();
            $cacah = 0;
            $data = [];

            foreach ($Product as $i => $d) {
                $data[$cacah] = [
                  $d->images1,
                  $d->name,
                  "Rp. ".number_format($d->harga,0,",","."),
                    $d->stock,
                  $d->category['name'], 
                  $d->cluster['name'], 
                  $d->id 
                ];

                $cacah++;    
            }

            return response()->json([
                'data' => $data
            ]);
        }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Product $product)
    {
      $this->validate($request, [
            'name' => 'required',
            'harga' => 'required',

      ]);

           $tambah = new product();
           $tambah->id = Uuid::uuid4();
           $tambah->name = $request['name'];
           $tambah->finishing = $request['finishing'];
           $tambah->material = $request['material'];
           $tambah->harga = $request['harga'];
           $tambah->discon = $request['discon'];
           $tambah->spesification = $request['spesification'];
           $tambah->description = $request['description'];
           $tambah->long_des = $request['long_des'];
           $tambah->id_category = $request['category'];
           $tambah->id_cluster = $request['cluster'];
           $tambah->stock = $request['stock'];
  

          $upload_path ='images/product';
           //1 
           if($request->file('foto1') != ""){

           $file1       = $request->file('foto1');
           $ext1        = $file1->getClientOriginalExtension();
           $fileName1   = "product-".date('YmdHis')."1.$ext1";
           $request->file('foto1')->move($upload_path, $fileName1);
           $tambah->images1 = $fileName1;
         }

           if($request->file('foto2') != ""){

           //2 
           $file2       = $request->file('foto2');
           $ext2        = $file2->getClientOriginalExtension();
           $fileName2   = "product-".date('YmdHis')."2.$ext2";
           $request->file('foto2')->move($upload_path, $fileName2);
           $tambah->images2 = $fileName2;
           }
           //3
           if($request->file('foto3') != ""){
             $file3       = $request->file('foto3');
             $ext3       =$file3->getClientOriginalExtension();
             $fileName3   = "product-".date('YmdHis')."3.$ext3";
             $request->file('foto3')->move($upload_path, $fileName3);
             $tambah->images3 = $fileName3;
           }
           //4
           if($request->file('foto4') != ""){
             $file4       = $request->file('foto4');
             $ext4       =$file4->getClientOriginalExtension();
             $fileName4   = "product-".date('YmdHis')."4.$ext4";
             $request->file('foto4')->move($upload_path, $fileName4);
             $tambah->images4 = $fileName4;
           }
          
      
      
           $tambah->save();

           return redirect()->to('/admin/product')
                            ->with('success','Item updated successfully');

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $clusters = Cluster::All();
        $categorys = Category::All();
        $tampiledit = Product::where('id', $id)->first();
        return view('admin.product.edit')->with('tampiledit', $tampiledit)->with('clusters',$clusters)->with('categorys',$categorys);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function update(Request $request, $id)
    {
           $update = Product::where('id', $id)->first();
           $update->name = $request['name'];
           $update->finishing = $request['finishing'];
            $update->stock = $request['stock'];
           $update->material = $request['material'];
           $update->harga = $request['harga'];
           $update->discon = $request['discon'];
           $update->spesification = $request['spesification'];

           $update->description = $request['description'];
           $update->long_des = $request['long_des'];

           $update->id_category = $request['category'];
      

        $upload_path ='images/product';

       if($request->file('foto1') == "")
        {
            $update->images1 = $update->images1;
        } 
        else
        {
           $file1       = $request->file('foto1');
           $ext1        = $file1->getClientOriginalExtension();
           $fileName1   = "product-".date('YmdHis')."1.$ext1";
           $request->file('foto1')->move($upload_path, $fileName1);
           $tambah->images1 = $fileName1;
        }
        if($request->file('foto2') == "")
        {
            $update->images2 = $update->images2;
        } 
        else
        {
            $file2       = $request->file('foto2');
           $ext2        = $file2->getClientOriginalExtension();
           $fileName2   = "product-".date('YmdHis')."2.$ext2";
           $request->file('foto2')->move($upload_path, $fileName2);
           $tambah->images2 = $fileName2;
        }
        
        //FOTO 3
        if($request->file('foto3') == "")
        {
            $update->images3 = $update->images3;
        } 
        else
        {
           $file3       = $request->file('foto3');
           $ext3       =$file3->getClientOriginalExtension();
           $fileName3   = "product-".date('YmdHis')."3.$ext3";
           $request->file('foto3')->move($upload_path, $fileName3);
           $tambah->images3 = $fileName3;
        }
        
         //FOTO4 
        if($request->file('foto4') == "")
        {
            $update->images4 = $update->images4;
        } 
        else
        {
          $file4       = $request->file('foto4');
           $ext4       =$file4->getClientOriginalExtension();
           $fileName4   = "product-".date('YmdHis')."4.$ext4";
           $request->file('foto4')->move($upload_path, $fileName4);
           $tambah->images4 = $fileName4;
        }
      
        $update->update();

        return redirect()->to('/admin/product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hapus = Product::find($id);
        $hapus->delete();

        return redirect()->to('/admin/product');
    }
}
