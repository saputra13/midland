<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Cluster;
use Ramsey\Uuid\Uuid;
use Yajra\Datatables\Datatables;
use Response;

class ClusterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.cluster.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin.cluster.create');
    }


    public function getdatacluster() {
            $cluster = Cluster::all();
            $cacah = 0;
            $data = [];

            foreach ($cluster as $i => $d) {
                $data[$cacah] = [
                  $d->name,
                  $d->id 
                ];

                $cacah++;    
            }

            return response()->json([
                'data' => $data
            ]);
        }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Cluster $cluster)
    {
         $this->validate($request, [
            'name' => 'required',
      ]);

           $tambah = new Cluster();
           $tambah->id = Uuid::uuid4();
           $tambah->name = $request['name'];
           $tambah->save();

           return redirect()->to('/admin/cluster')
                            ->with('success','Item updated successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tampiledit = cluster::where('id', $id)->first();
        return view('admin.cluster.edit')->with('tampiledit', $tampiledit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function update(Request $request, $id)
    {
        $update = cluster::where('id', $id)->first();
        $update->name = $request['name'];
      
        $update->update();

        return redirect()->to('/admin/cluster');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hapus = cluster::find($id);
        $hapus->delete();

        return redirect()->to('/admin/cluster');
    }
}
