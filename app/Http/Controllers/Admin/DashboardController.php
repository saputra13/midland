<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Cluster;
use App\Model\Category;
use App\Model\Analytic;
use App\Model\Product;

use Ramsey\Uuid\Uuid;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use Response;

class DashboardController extends Controller
{
    
    public function index()
    {
        $total = Analytic::All();
        $product = Product::All();
        $category = Category::All();
        return view('admin.index')->with('total',$total)->with('product',$product)->with('category',$category);
    }

    public function getdatapopuler() {
            $Product = DB::table('product')
                                ->join('analytic','product.id','=','analytic.id_product') 
                                ->select(DB::raw('count(analytic.id_product) as jumlah'),'images1','harga','discon','product.id','name')
                                ->groupBy('id_product')
                                ->orderByRaw('jumlah')
                                ->get();


            $cacah = 0;
            $data = [];

            foreach ($Product as $i => $d) {
                $data[$cacah] =
                 [
                  $d->jumlah,
                  $d->images1,
                  $d->name,
                  "Rp. " . number_format($d->harga,0,".","."),
                "Rp. " . number_format($d->discon,0,".","."),
                  $d->id 
                ];

                $cacah++;    
            }

            return response()->json([
                'data' => $data
            ]);
        }

    
}
