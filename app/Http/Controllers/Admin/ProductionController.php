<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\Production;
use Carbon\Carbon;
use Steevenz\Rajaongkir;
use Response;
use PDF;


class ProductionController extends Controller
{
    public function index()
    {
    	$terbayar = Order::where('status','TERBAYAR')->get();
    	$produksi = Order::where('status','PRODUKSI')->get();	
    	$kirim = Order::where('status','KIRIM')->get();	
      return view('admin.production.production')->with('terbayar',$terbayar)->with('produksi',$produksi)->with('kirim',$kirim);
    }

    public function productiondetail(Request $request, $id)
    {
      
        $order = Order::findorFail($id);
        return view('admin.production.production-detail',compact('order')); 
      }

    public function productiondetailproduct(Request $request, $id)
    {
        $orderdetail = OrderDetail::findorFail($id);
        return view('admin.production.production-detail-product', compact('orderdetail'));
    }
  
    public function getdataproductionorder(){
        $order = order::where('status','TERBAYAR')->get();
        $cacah = 0;
        $data = [];

        foreach ($order as $i => $d) {
            $data[$cacah] = [
              $d->order_number,
              $d->member->name,
              $d->member->phone,
              $d->order_date,
              $d->status,
              $d->id, 
              $d->id 
            ];

            $cacah++;    
        }

        return response()->json([
            'data' => $data
        ]);
    }
    public function getdataproductionwip(){
        $order = order::where('status','PRODUKSI')->get();
        $cacah = 0;
        $data = [];

        foreach ($order as $i => $d) {
            $data[$cacah] = [
               $d->order_number,
              $d->member->name,
              $d->member->phone,
              $d->order_date,
              $d->status,
              $d->id 
            ];

            $cacah++;    
        }

        return response()->json([
            'data' => $data
        ]);
    }
    public function getdataproductiondone(){
        $order = order::where('status','KIRIM')->get();
        $cacah = 0;
        $data = [];

        foreach ($order as $i => $d) {
            $data[$cacah] = [
              $d->order_number,
              $d->member->name,
              $d->member->phone,
              $d->order_date,
              $d->status,
              $d->id 
            ];

            $cacah++;    
        }

        return response()->json([
            'data' => $data
        ]);
    }
  
   public function update(Request $request, $id){
        $status = $request['status'];
        $order = Order::where('id',$id)->first();
        $order->payment_date = Carbon::now();
        $order->status = $status;
        $order->update();
        return back(); 
    }
  public function getspk2(Request $request, $id)
    {
     $order = Order::where('id',$id)->get();
     //$pdf = PDF::loadView('admin.production.spk', compact('order'))
    //            ->setPaper('a4','potrait');
    //return $pdf->stream("dompdf_out.pdf", array("Attachment" => false));
    //exit(0);
   //return $order;
      $campaign = Campaign::with(['store','feedback.member', 'design.mockup','design.artdesign','design.modelproduct'])->findorFail($campaign_id);
		return view('detail-campaign-editor')->with('campaign', json_decode($campaign, true));
    return view('admin.production.spk')->with('order', json_decode($campaign, true));
    }
  public function getspk(Request $request, $id)
    {
     $order = OrderDetail::findorFail($id);
     $pdf = PDF::loadView('admin.production.spk', compact('order'))
                ->setPaper('a4','potrait');
    return $pdf->stream("dompdf_out.pdf", array("Attachment" => false));
    exit(0);
   }
  
    public function show(){
      
    }
     public function timeline(Request $request, $id){
      $production = Production::where('id_order',$id)->get();
      $order = Order::findorFail($id);
      return view('admin.production.production-timeline',compact('production','order'));
    }
    public function upload(Request $request){
      $production = new Production();
      $production->keterangan = $request['keterangan'];
      $id_order = $request['id_order'];
      $production->id_order = $id_order;
      $production->date = carbon::now();

       $upload_path ='images/production';
           //1 
           if($request->file('images') != ""){
           $file       = $request->file('images');
           $ext        = $file->getClientOriginalExtension();
           $fileName   = "production-".$id_order."-".date('YmdHis').".$ext";
           $request->file('images')->move($upload_path, $fileName);
           $production->images = $fileName;
         }
      $production->save();
      return redirect()->back();
    }
}
