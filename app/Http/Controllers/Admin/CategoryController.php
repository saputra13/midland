<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\Cluster;
use Ramsey\Uuid\Uuid;
use Yajra\Datatables\Datatables;
use Response;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     $categorys = Category::All();
      return view('admin.category.create')->with('categorys',$categorys);
    }

    public function getdatacategory() {
            $category = Category::all();

            $cacah = 0;
            $data = [];

            foreach ($category as $i => $d) {
                $data[$cacah] = [
                  $d->name,
                  $d->images,
                  $d->id 
                ];

                $cacah++;    
            }

            return response()->json([
                'data' => $data
            ]);
        }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Category $category)
    {
         $this->validate($request, [
            'name' => 'required',
      ]);

           $tambah = new Category();
           $tambah->id = Uuid::uuid4();
           $tambah->name = $request['name'];

           $file       = $request->file('images');
           $ext        = $file->getClientOriginalExtension();
           $fileName   = "category-".date('YmdHis').".$ext";
           $upload_path ='images/category';
           $request->file('images')->move($upload_path, $fileName);
           $tambah->images = $fileName;
      
           $tambah->save();

           return redirect()->to('/admin/category')
                            ->with('success','Item updated successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tampiledit = Category::where('id', $id)->first();
        $clusters = Cluster::All();
        return view('admin.category.edit')->with('tampiledit', $tampiledit)->with('clusters',$clusters);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function update(Request $request, $id)
    {
        $update = Category::where('id', $id)->first();
        $update->name = $request['name'];
        if($request->file('images') == "")
        {
            $update->icon = $update->icon;
        } 
        else
        {
            $file       = $request->file('images');
            $ext       =$file->getClientOriginalExtension();
            $fileName   = "category-".date('YmdHis').".$ext";
            $upload_path ='images/category';
            $request->file('images')->move($upload_path, $fileName);
            $update->images = $fileName;
        }
      
        $update->update();

        return redirect()->to('/admin/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hapus = Category::find($id);
        $hapus->delete();

        return redirect()->to('/admin/category');
    }
}
