<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\ShippingCost;
use App\Model\States;
use App\Model\Cities;
use Ramsey\Uuid\Uuid;
use Yajra\Datatables\Datatables;
use Response;

class ShippingCostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.shipping.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $state = States::all();
      return view('admin.shipping.create',compact('state'));
    }


    public function getdatacity(Request $request) {
        $parent_id = $request['state_id'];
        $city = Cities::where('state_id',$parent_id)->orderBy('name')->get();
        return $city;
    }

    public function getdatashipping() {
            $Shipping = ShippingCost::all();
            $cacah = 0;
            $data = [];

            foreach ($Shipping as $i => $d) {
                $data[$cacah] = [
                  $d->city,
                  "$ " . $d->cost,
                  $d->id 
                ];

                $cacah++;    
            }

            return response()->json([
                'data' => $data
            ]);
        }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, [
            'city' => 'required',
            'cost' => 'required',
      ]);

           $tambah = new ShippingCost();
           $tambah->id = Uuid::uuid4();
           $tambah->city = $request['city'];
           $tambah->cost = $request['cost'];
           $tambah->save();

           return redirect()->to('/admin/shipping')
                            ->with('success','Item updated successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tampiledit = ShippingCost::where('id', $id)->first();
        return view('admin.Shipping.edit')->with('tampiledit', $tampiledit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function update(Request $request, $id)
    {
        $update = ShippingCost::where('id', $id)->first();
        $update->city = $request['city'];
        $update->cost = $request['cost'];
      
        $update->update();

        return redirect()->to('/admin/shipping');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hapus = ShippingCost::find($id);
        $hapus->delete();

        return redirect()->to('/admin/shipping');
    }
}
