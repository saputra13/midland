<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


use App\Http\Requests;
use App\Model\Address;
use App\Model\Shipping;
use App\Model\ShippingCost;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\Geo;
use App\Model\Cities;
use App\Model\States;

use App\Model\Product;
use App\Member;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use \Cart as Cart;
use Validator;
use Response;
use Alert;

use PayPal\Api\Amount;
use PayPal\Api\ShippingAddress;
use PayPal\Api\Details;
use PayPal\Api\Item;
/** All Paypal Details class **/
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use Session;
use URL;
class CheckoutController extends Controller
{
   private $_api_context;
   


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);
    }
    public function index()
    {
        
         $state = States::all();
        return view('checkout',compact('state'));
    }
    public function getdatacity(Request $request) {
        $parent_id = $request['state_id'];
        $city = Cities::where('state_id',$parent_id)->orderBy('name')->get();
        return $city;
    }

    public function getcost(Request $request) {
        $city_name = $request['city_name'];
        $cost = ShippingCost::where('city',$city_name)->first();
        return $cost;
    }
  

  //CHECKOUT PAYMENT 

    public function payment(Request $request)
    {
        //FORM FIELD

        $name = $request['name'];
        $phone = $request['phone'];
        $email = $request['email'];
        $company = $request['company'];
        $country = $request['country'];
        $state_id = $request['state'];
        $state = States::where('id',$state_id)->first();
        $state_name = $state['name'];
        $city = $request['city'];
        $zip = $request['zip'];
        $address1 = $request['address1'];
        $address2 = $request['address2'];
        $description = $request['description'];
        $ongkir = $request['ongkir'];
        $nominal_payment = $request['nominal_payment'];
        $total_payment = $request['total_payment'];
        $payment_method = $request['payment_method'];

        if($payment_method === 'PAYPAL'){
            Session::flash('name',$name);
            Session::flash('phone',$phone);
            Session::flash('email',$email);
            Session::flash('company',$company);
            Session::flash('country',$country);
            Session::flash('state_name',$state_name);
            Session::flash('city',$city);
            Session::flash('zip',$zip);
            Session::flash('address1',$address1);
            Session::flash('address2',$address2);
            Session::flash('description',$description);
            Session::flash('ongkir',$ongkir);
            Session::flash('nominal_payment',$nominal_payment);
            Session::flash('total_payment',$total_payment);


            $items = [];
            $payer = new Payer();
            $payer->setPaymentMethod('paypal');
            foreach (Cart::content() as $i => $x) {
                  $item = new Item();
                    $item->setName($x->name) /** item name **/
                        ->setCurrency('USD')
                        ->setQuantity($x->qty)
                        ->setPrice($x->price); /** unit price **/
                        $items[] = $item;
            }
          
          
            $item_list = new ItemList();
            $item_list->setItems($items);
             $shippingAddress = [
                "recipient_name" => $request['name'],
                "line1" => $request['address1'],
                "line2" => $request['address2'],
                "city" => $request['city'],
                "country_code" => "US",
                "postal_code" => $request['zip'],
                "state" => $state['name'],
                "phone" => $request['phone']
            ];
            

            $item_list->setShippingAddress($shippingAddress);

            $amount = new Amount();
            $amount->setCurrency('USD')
                ->setDetails(array('subtotal'=>$request['total_payment'],'shipping'=>$request['ongkir']))
                ->setTotal($request['nominal_payment']);
            $transaction = new Transaction();
            $transaction->setAmount($amount)
                ->setItemList($item_list)
                ->setDescription('Your transaction description');
            $redirect_urls = new RedirectUrls();
            $redirect_urls->setReturnUrl(URL::to('status')) /** Specify return URL **/
                ->setCancelUrl(URL::to('status'));
            $payment = new Payment();
            $payment->setIntent('Sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));
            /** dd($payment->create($this->_api_context));exit; **/
            try {
                $payment->create($this->_api_context);
            } catch (\PayPal\Exception\PayPalConnectionException $ex) {
                  \Session::put('error', 'Value is invalid');
                return Redirect::back()->withErrors([$ex->getData()])->withInput(Input::all());
            }
            foreach ($payment->getLinks() as $link) {
                if ($link->getRel() == 'approval_url') {
                    $redirect_url = $link->getHref();
                    break;
                }
            }
            /** add payment ID to session **/
            Session::put('paypal_payment_id', $payment->getId());
            if (isset($redirect_url)) {
                /** redirect to paypal **/
                return Redirect::away($redirect_url);
            }
            \Session::put('error', 'Unknown error occurred');
            return Redirect::to('/');
        }elseif($payment_method ==='COD'){
          
            $userid =  Auth::guard('member')->user()->id;
            $member = Member::where('id',$userid)->first();
            $member_id = $member['id'];

           

            $address = new Address();
            $address->state =  $state_name;
            $address->country =  $country;
            $address->city =  $city;
            $address->address1 =  $address1;
            $address->address2 =  $address2;
            $address->zip =  $zip;
            $address->Save();

            $shipping = new Shipping();
            $shipping->name = $name;
            $shipping->phone = $phone;
            $shipping->email = $email;
            $shipping->company = $company;
            $address->shipping()->save($shipping);


            $gen =  Uuid::uuid4();
            $date = Carbon::now();
            $order_date = Carbon::now()->toDateTimeString();
            $invoice = "INV." . $date->format('mdY') . "." .strtoupper((substr($gen, 29))); 
          
            Session::put('invoice',$invoice);
            $order = new Order;
            $order->order_number = $invoice;
            $order->description = $description;
            $order->order_date = $order_date;
            $order->payment_date = $order_date;
            $order->payment_method = 'COD';
            $order->nominal_payment = Cart::total();
            $order->ongkir = $ongkir;
            $order->grand_total = $nominal_payment;
            $order->status = 'PENDING';
            $order->id_member = $member_id;
            $shipping->order()->save($order);
          
            foreach(Cart::content() as $row) {
              $orderDetail = new OrderDetail();
              $orderDetail->qty = $row->qty;
              $orderDetail->total = ($row->qty) * ($row->price);
              $orderDetail->id_product = $row->id;
              $order->orderDetail()->save($orderDetail);
            }
            Cart::destroy();

            \Session::put('success', 'Payment success');
            return Redirect::to('/landing');

        }else{
            \Session::put('error', 'Value is invalid');
            return Redirect::back();
        }

        
    }

    public function getPaymentStatus()
    {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            \Session::put('error', 'Payment failed');
            return Redirect::to('/landing');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        
        
        try{
            $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        }catch(\Exception $e) {
             // print_r($e);
            // or
            print_r (json_decode($e->getData(), true));
            die($e);
        }
        
        
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
        if ($result->getState() == 'approved') {

            //INSERT TO DATABASE

            $name = Session::get('name');
            $phone = Session::get('phone');
            $email = Session::get('email');
            $company = Session::get('company');
            $country = Session::get('country');
            $state_name = Session::get('state_name');
            $city = Session::get('city');
            $zip = Session::get('zip');
            $address1 = Session::get('address1');
            $address2 = Session::get('address2');
            $description = Session::get('description');
            $ongkir = Session::get('ongkir');
            $nominal_payment = Session::get('nominal_payment');
            $grand_total = Session::get('total_payment');


            $userid =  Auth::guard('member')->user()->id;
            $member = Member::where('id',$userid)->first();
            $member_id = $member['id'];

           

            $address = new Address();
            $address->state =  $state_name;
            $address->country =  $country;
            $address->city =  $city;
            $address->address1 =  $address1;
            $address->address2 =  $address2;
            $address->zip =  $zip;
            $address->Save();

            $shipping = new Shipping();
            $shipping->name = $name;
            $shipping->phone = $phone;
            $shipping->email = $email;
            $shipping->company = $company;
            $address->shipping()->save($shipping);


            $gen =  Uuid::uuid4();
            $date = Carbon::now();
            $order_date = Carbon::now()->toDateTimeString();
            $invoice = "INV." . $date->format('mdY') . "." .strtoupper((substr($gen, 29))); 
            
            Session::put('invoice',$invoice);
            
            $order = new Order;
            $order->order_number = $invoice;
            $order->description = $description;
            $order->order_date = $order_date;
            $order->payment_date = $order_date;
            $order->payment_method = 'PAYPAL';
            $order->nominal_payment = Cart::total();
            $order->ongkir = $ongkir;
            $order->grand_total = $grand_total;
            $order->status = 'PAID';
            $order->id_member = $member_id;
            $shipping->order()->save($order);
          
            foreach(Cart::content() as $row) {
              $orderDetail = new OrderDetail();
              $orderDetail->qty = $row->qty;
              $orderDetail->total = ($row->qty) * ($row->price);
              $orderDetail->id_product = $row->id;
              $order->orderDetail()->save($orderDetail);
            }
            Cart::destroy();


            \Session::put('success', 'Payment success');
            return Redirect::to('/landing');
        }else{
         \Session::put('error', 'Payment failed');
          return Redirect::to('/landing');
        }
        
    }
}