<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Product;
use App\Model\Category;
use App\Model\Cluster;
use App\Model\Analytic;
use Ramsey\Uuid\Uuid;
use Yajra\Datatables\Datatables;
use Response;	


class ProductDetailController extends Controller
{
    public function index($id)
    {

    		$tambah = new Analytic();
           	$tambah->id = Uuid::uuid4();
           	$tambah->id_product = $id;
           	$tambah->save();

			 $product = Product::with(['category','cluster'])->findorFail($id);
			 $products = Product::take(6)->get();

			//return view('detail-product')->with('product', json_decode($product, true))->with('products', $products );
    	 	return $product;
    }
}
