<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Model\Product;
use App\Member;
use App\Model\Address;
use App\Model\Shipping;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use \Cart as Cart;
use Validator;
use Response;
use Alert;

class ShippingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function add(Request $request)
      {
          //Cart::destroy();

       $userid =  Auth::guard('member')->user()->id;
       $member = Member::where('id',$userid)->first();
       $member_id = $member['id'];

          $name = $request['name'];
          $phone = $request['phone'];
          $email = $request['email'];
          $city = $request['city'];
          $zip = $request['zip'];
          $company = $request['company'];

          $input = $request->all();
          $address = Address::create($input);

          $shipping = new Shipping();
          $shipping->name = $name;
          $shipping->phone = $phone;
          $shipping->email = $email;
          $shipping->company = $company;
          $address->shipping()->save($shipping);


          $gen =  Uuid::uuid4();
          $date = Carbon::now();
          $order_date = Carbon::now()->toDateTimeString();
          $invoice = "INV." . $date->format('mdY') . "." .strtoupper((substr($gen, 29))); 
          
          $order = new Order;
          $order->order_number = $invoice;
          $order->order_date = $order_date;
          $order->nominal_payment = Cart::total();
          $order->status = 'ORDER';
          $order->id_member = $member_id;
          $shipping->order()->save($order);
          
            foreach(Cart::content() as $row) {
              $orderDetail = new OrderDetail();
              $orderDetail->qty = $row->qty;
              $orderDetail->total = ($row->qty) * ($row->price);
              $orderDetail->id_product = $row->id;
              $order->orderDetail()->save($orderDetail);
            }
            Cart::destroy();
            $link = 'berhasil';
            return redirect($link);
      }

    public function complete(Request $request, $id)
    {
      $order = Order::where('order_number',$id)->first();
      $order_id = $order['id'];
      $orderdetail = OrderDetail::where('id_order',$order_id)->with('product')->get();
      return view('complete', compact('order','orderdetail'));
    }
    public function show()
    {
      
    }

}
