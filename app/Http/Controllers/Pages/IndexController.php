<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Product;
use App\Model\Category;
use App\Model\Slider;
use App\Model\OrderDetail;
use Illuminate\Support\Facades\DB;


class IndexController extends Controller
{
  
    public function index()
    {
    	$slider = Slider::where('active','active')->get();
		$chair = Product::where('id_category','42cdf34d-f448-4747-a5f6-d9001a5aa74f')->whereNotNull('images1')->whereNotNull('images1')->take(10)->get();
		$kitchen = Product::where('id_category','23e782ff-e21c-4576-a2f5-7486eedf2561')->whereNotNull('images1')->take(10)->get();
		$sofa = Product::where('id_category','673431f4-ce9e-4768-9a99-a2918dbf6a0f')->whereNotNull('images1')->take(10)->get();
		$consul = Product::where('id_category','aaaf09d2-cb02-488a-92e1-798118a52882')->whereNotNull('images1')->take(10)->get();
		$nakas = Product::where('id_category','ac8938f1-5934-409a-b362-95add79bf3b0')->whereNotNull('images1')->take(10)->get();
		$table = Product::where('id_category','c0df46a1-c164-40c5-b33d-d53b77fb657a')->whereNotNull('images1')->take(10)->get();
		$new = Product::whereNotNull('images1')->orderBy('created_at','ASC')->take(8)->get();
       
    return view('index',compact('chair','kitchen','sofa','consul','nakas','table','slider','new'));
	}
	
	public function profile(){
		return view('profile');
	}
  
}
