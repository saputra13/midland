<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\Production;
use App\Http\Controllers\Controller;


class TrackController extends Controller
{
  
    public function index(Request $request)
    {
    	$order_id = $request['id_order'];
    	$order = Order::where('order_number',$order_id)->first();
    	$id_order = $order['id'];
    	$production = Production::where('id_order',$id_order)->get();
        return view('track',compact('production','order'));
    }
  
}
