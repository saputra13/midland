<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Model\Product;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use \Cart as Cart;
use Validator;
use Illuminate\Support\Facades\Auth;
use Alert;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Cart::destroy();
        return view('cart');
    }
    public function add(Request $request)
      {
         // if(Auth::guest()){
          //  Alert::warning('Atau daftar bagi yang tidak punya akun','Silahkan //Login untuk melanjutkan Transaksi');
          //  return redirect('login');
         // }else{
            $id = $request['id'];
            $name = $request['name'];
            $sale_price = $request['sale_price'];
            $product = Product::where('id',$id)->first();
            $images1 = $product['images1'];
            Cart::add(['id' => $id, 'name' => $name, 'qty' => 1, 'price' => $sale_price, 'options' => ['images1' => $images1]]);
            return redirect('cart');
        //  }
          //Cart::destroy();
          
      }
       public function update(Request $request)
        {
            $id = $request['id'];
            $qty = $request['qty'];
            Cart::update($id, $qty);

        }
    public function show()
    {
      
    }

}
