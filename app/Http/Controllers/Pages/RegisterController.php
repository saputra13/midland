<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Member;
use App\Model\Address;
use Carbon\Carbon;
use Validator;
use Ramsey\Uuid\Uuid;
use Alert;
use Mail;

class RegisterController extends Controller
{
    public function __construct()
    {
        //$this->middleware('guest', ['except' => 'logout']);
    }
  
    public function index()
    {
        return view('sign-up');
    }
  
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'   => 'required|email|unique:user',
            'password'=> 'required|min:6',
        ], [
            'email.required' => 'Email diperlukan!',
            'password.required' => 'Password diperlukan'
        ]);

        if ($validator->fails()) {
            return redirect('/register')
                        ->withErrors($validator)
                        ->withInput();
        } else {
                $user = new User();
                $user->email = $request['email'];
                $user->password = bcrypt($request['password']);
                $user->level = $request['level'];
                $user->username = $request['username'];
                $user->save();
                $member = new Member();
                $member->datetime = Carbon::now();
                $member->firstname = $request['firstname'];
                $member->lastname = $request['lastname'];
                $member->phone = $request['phone'];
                $member->status = 'ACTIVE';
          
                $user->member()->save($member);
                $data = array( 'email' => $request['email']);
                Mail::send('mail.register', $data, function($message) use ($data) {
                    $message->to('anggisptr110@gmail.com');
                    $message->from('no-reply@mootivo.com', 'Admin Mootivo');
                    $message->subject('Verifikasi Email Akun Mootivo');
                });
                Alert::success('Selamat bergabung menjadi member mootivo', 'Registrasi Berhasil!');
                return redirect('login');
              }
     }
    
}
