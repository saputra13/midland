<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Product;
use App\Model\Cluster;
use App\Model\Category;
use App\Model\Slider;
use Illuminate\Support\Facades\DB;


class CatalogController extends Controller
{
  
    public function index()
    {
		$products = Product::paginate(12);
		$categories = Category::orderBy('name')->get();
		//$cluster = Cluster::with('product.category')->get();
		//$discons = Product::All();
        return view('catalog')->with('products', $products)->with('categories', $categories);

    }


    public function category($name)
    {
		$products = Product::whereHas('category', function($q) use($name){
		    $q->where('name','=',$name);
		})->paginate(12);
		$categories = Category::All();
		//$discons = Product::All();
        return view('catalog')->with('products', $products)->with('categories', $categories);;
    }
  public function productdetail($id)
    {

    	
			 $product = Product::with(['category','cluster'])->findorFail($id);
			 $products = Product::take(6)->get();

			return view('product')->with('product', json_decode($product, true))
										 ->with('products', $products );
    }
}
