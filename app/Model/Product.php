<?php

namespace App\Model;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use Uuid;

    protected $table = 'product';

    protected $primaryKey = 'id';

     protected $fillable = [
		'id',
		'name',
        'finishing',
        'material',
        'harga',
        'discon',
        'spesification',
        'description',
        'long_des',
         'stock',
        'images1',
        'images2',
        'images3',
        'images4',
        'id_category',
        'id_cluster',
    ];

    public $incrementing = false;

    public function category()
    {
        return $this->belongsTo('App\Model\Category', 'id_category');
    }
    public function cluster()
    {
        return $this->belongsTo('App\Model\Cluster', 'id_cluster');
    }
     public function product()
    {
        return $this->hasMany('App\Model\Product', 'id_product');
    }
  
}