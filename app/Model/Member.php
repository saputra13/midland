<?php

namespace App\Model;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    use Uuid;

    protected $table = 'member';

    protected $primaryKey = 'id';

    protected $fillable = [
		'id',
		'firstname',
		'lastname',
		'phone',
		'avatar',
		'datetime',
		'status',
		'id_users',
		'id_address',
    ];

    public $incrementing = false;

    public function users()
    {
    	return $this->belongsTo('App\User', 'id_users');
    }

    public function address()
    {
    	return $this->belongsTo('App\Model\Address', 'id_address');
    }
   
}
