<?php

namespace App\Model;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class Analytic extends Model
{
    use Uuid;

    protected $table = 'analytic';

    protected $primaryKey = 'id';

     protected $fillable = [
		'id',
        'id_product',
    ];

    public $incrementing = false;

    public function product()
    {
        return $this->belongsTo('App\Model\Product', 'id_product');
    }
}