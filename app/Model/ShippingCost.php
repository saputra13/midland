<?php

namespace App\Model;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class ShippingCost extends Model
{
    use Uuid;

    protected $table = 'shipping_cost';

    protected $primaryKey = 'id';

    protected $fillable = [
		'id',
        'city',
        'cost',
    ];

    public $incrementing = false;
 
}
