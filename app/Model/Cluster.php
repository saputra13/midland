<?php

namespace App\Model;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class Cluster extends Model
{
    use Uuid;

    protected $table = 'cluster';

    protected $primaryKey = 'id';

     protected $fillable = [
		'id',
		'name',
    ];

    public $incrementing = false;

 	 public function product()
    {
        return $this->hasMany('App\Model\Category', 'id_category');
    }
}
