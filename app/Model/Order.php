<?php

namespace App\Model;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use Uuid;

    protected $table = 'order';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'order_number',
        'order_date',
        'payment_date',
        'description',
        'nominal_payment',
        'payment_method',
        'ongkir',
        'status',
        'description',
        'grand_total',
        'id_member',
        'id_shipping',
    ];

    public $incrementing = false;

  

    public function productiondetail()
    {
        return $this->hasMany('App\Model\ProductionDetail', 'id_order');
    }
    public function member()
    {
        return $this->belongsTo('App\Member', 'id_member');
    }
    public function shipping()
    {
        return $this->belongsTo('App\Model\Shipping', 'id_shipping');
    }
    public function orderdetail()
    {
        return $this->hasMany('App\Model\OrderDetail', 'id_order');
    }
}
