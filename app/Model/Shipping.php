<?php

namespace App\Model;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    use Uuid;

    protected $table = 'shipping';

    protected $primaryKey = 'id';

    protected $fillable = [
		'id',
        'name',
        'phone',
        'email',
        'company',
        'id_address',
    ];

    public $incrementing = false;

    public function address()
    {
    	return $this->belongsTo('App\Model\Address', 'id_address');
    }
    public function order()
    {
        return $this->hasMany('App\Model\Order', 'id_shipping');
    }
}
