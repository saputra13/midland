<?php

namespace App\Model;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use Uuid;

    protected $table = 'address';

    protected $primaryKey = 'id';

    protected $fillable = [
		'id',
        'country',
        'address1',
        'address2',
        'state',
        'city',
        'zip',
    ];

    public $incrementing = false;

    public function member()
    {
    	return $this->hasOne('App\Model\Buyer', 'id_address');
    }
    public function shipping()
    {
        return $this->hasOne('App\Model\shipping', 'id_address');
    }
}
