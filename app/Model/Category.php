<?php

namespace App\Model;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Uuid;

    protected $table = 'category';

    protected $primaryKey = 'id';

     protected $fillable = [
		'id',
        'name',
		'images',
    ];

    public $incrementing = false;

   
    public function product()
    {
        return $this->hasMany('App\Model\Product', 'id_category');
    }
}