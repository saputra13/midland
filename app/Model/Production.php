<?php

namespace App\Model;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class Production extends Model
{
    use Uuid;

    protected $table = 'production';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'date',
        'keterangan',
        'images',
        'id_order',
    ];

    public $incrementing = false;

  

    public function order()
    {
        return $this->belongsTo('App\Model\Order', 'id_order');
    }
}
