<?php

namespace App\Model;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class Geo extends Model
{
    use Uuid;

    protected $table = 'geo';

    protected $primaryKey = 'id';

     protected $fillable = [
		'id',
        'parent_id',
        'left',
        'right',
        'depth',
        'name',
        'alternames',
        'country',
        'population',
        'lat',
		'long',
    ];

    public $incrementing = false;
}
