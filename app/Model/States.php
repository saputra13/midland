<?php

namespace App\Model;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class States extends Model
{
    use Uuid;

    protected $table = 'states';

    protected $primaryKey = 'id';

     protected $fillable = [
		'id',
        'name',
        'country_id',
    ];

    public $incrementing = false;
}
