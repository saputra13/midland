<?php

namespace App\Model;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    use Uuid;

    protected $table = 'order_detail';

    protected $primaryKey = 'id';

    protected $fillable = [
		'id',
		'qty',
		'total',
		'id_order',
		'id_product',
    ];

    public $incrementing = false;

    public function order()
    {
    	return $this->belongsTo('App\Model\Order', 'id_order');
    }
     public function product()
    {
        return $this->belongsTo('App\Model\Product', 'id_product');
    }

}
