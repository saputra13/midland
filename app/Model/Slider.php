<?php

namespace App\Model;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    use Uuid;

    protected $table = 'slider';

    protected $primaryKey = 'id';

     protected $fillable = [
		'id',
        'judul',
        'isi',
        'images',
        'slider',
		
    ];

    public $incrementing = false;
}