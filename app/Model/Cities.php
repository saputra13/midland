<?php

namespace App\Model;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class Cities extends Model
{
    use Uuid;

    protected $table = 'cities';

    protected $primaryKey = 'id';

     protected $fillable = [
		'id',
        'name',
        'state_id',
    ];

    public $incrementing = false;
}
