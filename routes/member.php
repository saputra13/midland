<?php

  Route::get('/', 'Member/HomeController@index')->name('home');
  Route::get('/order', 'Member\OrderController@index');
  Route::get('/order/payment/{id}', 'Member\OrderController@payment');
  Route::post('/order/update/{id}', 'Member\OrderController@update');
  Route::get('/order-detail/{id}', 'Member\OrderController@orderdetail');
  Route::get('/production-timeline/{id}', 'Member\OrderController@timeline'); 
