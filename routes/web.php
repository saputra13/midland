<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

///FRONT
Route::get('/', 'Pages\IndexController@index');
Route::get('/profile', 'Pages\IndexController@profile');
Route::get('/catalog', 'Pages\CatalogController@index');
Route::get('/catalog/category/{name}', 'Pages\CatalogController@category');

Route::get('/product/{id}', 'Pages\CatalogController@productdetail');
Route::get('/checkout', 'Pages\CheckoutController@index');
Route::get('/landing', 'Pages\LandingController@index');
Route::post('/checkout/payment', 'Pages\CheckoutController@payment');
Route::post('/getcity', 'Pages\CheckoutController@getdatacity');
Route::post('/getcost', 'Pages\CheckoutController@getcost');
Route::get('/cart', 'Pages\CartController@index');
Route::post('/cart/add', 'Pages\CartController@add');
Route::post('/cart/update', 'Pages\CartController@update');
Route::post('/shipping/add', 'Pages\ShippingController@add');
Route::get('/complete/{id}', 'Pages\ShippingController@complete');
Route::get('/payment/pay-credit-card', 'Pages\PaypalPaymentController@paywithCreditCard');


//paypal

//Route::get('/paypalpay', 'CheckoutController@index');
// route for processing payment
Route::post('paypal', 'Pages\CheckoutController@payWithpaypal');
// route for check status of the payment
Route::get('status', 'Pages\CheckoutController@getPaymentStatus')->name('status');


Route::group(['prefix' => 'admin'], function () {
  Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'AdminAuth\LoginController@login');
  Route::post('/logout', 'AdminAuth\LoginController@logout')->name('logout');

  //Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');
  //Route::post('/register', 'AdminAuth\RegisterController@register');

  Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');


 
});

Route::group(['prefix' => 'member'], function () {
 
  Route::get('/home', 'Member\HomeController@index');

  Route::get('/login', 'MemberAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'MemberAuth\LoginController@login');
  Route::post('/logout', 'MemberAuth\LoginController@logout')->name('logout');

  Route::get('/register', 'MemberAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'MemberAuth\RegisterController@register');

  Route::post('/password/email', 'MemberAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'MemberAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'MemberAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'MemberAuth\ResetPasswordController@showResetForm');


 
});
