<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('admin')->user();

    //dd($users);

    return view('admin.index');
})->name('home');

  Route::get('getdatapopuler', 'Admin\DashboardController@getdatapopuler');   
//CLUSTER
   Route::resource('cluster', 'Admin\ClusterController');
   Route::post('cluster/add', 'Admin\ClusterController@store');
   Route::get('getdatacluster', 'Admin\ClusterController@getdatacluster');
   Route::get('cluster/edit/{id}', 'Admin\ClusterController@edit');
   Route::post('cluster/update/{id}', 'Admin\ClusterController@update');
   Route::get('cluster/delete/{id}', 'Admin\ClusterController@destroy');

//SHIPPING COST
   Route::resource('shipping', 'Admin\ShippingCostController');
   Route::post('shipping/add', 'Admin\ShippingCostController@store');
   Route::get('getdatashipping', 'Admin\ShippingCostController@getdatashipping');
   Route::post('getcity', 'Admin\ShippingCostController@getdatacity');
   Route::get('shipping/edit/{id}', 'Admin\ShippingCostController@edit');
   Route::post('shipping/update/{id}', 'Admin\ShippingCostController@update');
   Route::get('shipping/delete/{id}', 'Admin\ShippingCostController@destroy');


//CATEGORY
   Route::resource('category', 'Admin\CategoryController');
   Route::post('category/add', 'Admin\CategoryController@store');
   Route::get('getdatacategory', 'Admin\CategoryController@getdatacategory');
   Route::get('category/edit/{id}', 'Admin\CategoryController@edit');
   Route::post('category/update/{id}', 'Admin\CategoryController@update');
   Route::get('category/delete/{id}', 'Admin\CategoryController@destroy');

   //PRODUCT
   Route::resource('product', 'Admin\ProductController');
   Route::post('product/add', 'Admin\ProductController@store');
   Route::get('getdataproduct', 'Admin\ProductController@getdataproduct');
   Route::get('product/edit/{id}', 'Admin\ProductController@edit');
   Route::post('product/update/{id}', 'Admin\ProductController@update');
   Route::get('product/delete/{id}', 'Admin\ProductController@destroy');

   //SLIDER
   Route::resource('slider', 'Admin\SliderController');
   Route::post('slider/add', 'Admin\SliderController@store');
   Route::get('getdataslider', 'Admin\SliderController@getdataslider');
   Route::get('slider/edit/{id}', 'Admin\SliderController@edit');
   Route::post('slider/update/{id}', 'Admin\SliderController@update');
   Route::get('slider/delete/{id}', 'Admin\SliderController@destroy');

   //Order
   Route::get('order', 'Admin\OrderController@index');
   Route::get('order/delete/{id}', 'Admin\OrderController@delete');
   Route::get('order-detail/{id}', 'Admin\OrderController@orderdetail');
   Route::post('order-detail/update/{id}', 'Admin\OrderController@update');

     //Production
    Route::get('/production', 'Admin\ProductionController@index');
    Route::get('/production-detail/{id}', 'Admin\ProductionController@productiondetail');
    Route::get('/production-timeline/{id}', 'Admin\ProductionController@timeline');
    Route::post('/production-timeline/upload/', 'Admin\ProductionController@upload');
    Route::get('getdataproductionorder', 'Admin\ProductionController@getdataproductionorder');
    Route::get('getdataproductionwip', 'Admin\ProductionController@getdataproductionwip');
    Route::get('getdataproductiondone', 'Admin\ProductionController@getdataproductiondone');
    Route::post('/production-detail/update/{id}', 'Admin\ProductionController@update');
    Route::get('/production-detail-product/spk/{id}', 'Admin\ProductionController@getspk');